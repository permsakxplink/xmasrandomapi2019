package com.xplink.api.rest.constant;

public class Constant {

	private Constant() {}
	
	public static final String LOG_PARM_UNKNOWN_VAL = "UNKNOWN";
	public static final String LOG_PARM_KEY_UUID = "uuid";
	public static final String LOG_PARM_KEY_BIZ_SERVICE = "bizService";
	public static final String LOG_PARM_KEY_HTTP_METHOD = "httpMethod";
	
	public static final int GROUP_STATE_READY = 0;
	public static final int GROUP_STATE_START = 1;
	public static final int GROUP_STATE_FINISH = 2;
	
}
