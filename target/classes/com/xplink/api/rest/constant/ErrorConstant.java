package com.xplink.api.rest.constant;

public class ErrorConstant {

	private ErrorConstant() {}
	
	public static final Integer STTS_SUCC = Integer.valueOf(0);
	public static final Integer STTS_ERROR = Integer.valueOf(-1);
	
	public static final String ERR_CODE_UNEXPECTED = "T1000";
	public static final String ERR_DESC_UNEXPECTED = "Unexpected error";
	public static final String ERR_CODE_REQS_BODY_NULL = "T1001";
	public static final String ERR_DESC_REQS_BODY_NULL = "Request body is null";
	public static final String ERR_CODE_CANNOT_EXEC_TASK = "T1002";
	public static final String ERR_DESC_CANNOT_EXEC_TASK = "Cannot execute background task";
	
	public static final String ERR_CODE_USER_NOT_FOUND = "B1000";
	public static final String ERR_DESC_USER_NOT_FOUND = "User is not found";
	public static final String ERR_CODE_WRONG_PASS = "B1001";
	public static final String ERR_DESC_WRONG_PASS = "Password is not correct";
	public static final String ERR_CODE_USER_EXPR_OR_TOKEN_NOT_FOUND = "B1002";
	public static final String ERR_DESC_USER_EXPR_OR_TOKEN_NOT_FOUND = "User has been expired or token is not found";
	public static final String ERR_CODE_INVD_TOKEN = "B1003";
	public static final String ERR_DESC_INVD_TOKEN = "Invalid token";
	public static final String ERR_CODE_KEYWORD_NOT_FOUND = "B1004";
	public static final String ERR_DESC_KEYWORD_NOT_FOUND = "Keyword is not found";
	public static final String ERR_CODE_RANDOM_NOT_FOUND = "B1005";
	public static final String ERR_DESC_RANDOM_NOT_FOUND = "Random is not found";
	public static final String ERR_CODE_GROUP_NOT_FOUND = "B1006";
	public static final String ERR_DESC_GROUP_NOT_FOUND = "Group is not found";
	public static final String ERR_CODE_GROUP_NAME_DUPLICATE = "B1007";
	public static final String ERR_DESC_GROUP_NAME_DUPLICATE = "Duplicate group name";
	public static final String ERR_CODE_EVENT_KEYWORD_DUPLICATE = "B1008";
	public static final String ERR_DESC_EVENT_KEYWORD_DUPLICATE = "Can't save this keyword";
	public static final String ERR_CODE_EVENT_USER_EXIST = "B1009";
	public static final String ERR_DESC_EVENT_USER_EXIST = "User already in Event Group";
	public static final String ERR_CODE_EVENT_NOT_FOUND_USER = "B1010";
	public static final String ERR_DESC_EVENT_NOT_FOUND_USER = "User is not found";
	public static final String ERR_CODE_USER_DUPLICATE = "B1011";
	public static final String ERR_DESC_USER_DUPLICATE = "User is Duplicate";
	
	public static final String ERR_CODE_CANNOT_DELETE_ADMIN = "B1012";
	public static final String ERR_DESC_CANNOT_DELETE_ADMIN = "You can not delete user ADMIN";
	public static final String ERR_CODE_YOU_CANNOT_DELETE = "B1013";
	public static final String ERR_DESC_YOU_CANNOT_DELETE = "You can not delete";
	public static final String ERR_CODE_ADMIN_CANNOT_CREATE = "B1014";
	public static final String ERR_DESC_ADMIN_CANNOT_CREATE = "Admin can not create group";
	public static final String ERR_CODE_NOT_THIS_USER = "B1015";
	public static final String ERR_DESC_NOT_THIS_USER = "You do not this user";
	
	public static final String ERR_CODE_NOT_DELETE_USER_EVENT = "B1016";
	public static final String ERR_DESC_NOT_DELETE_USER_EVENT = "Can not delete this user. Maybe user is in some Event Room";
	public static final String ERR_CODE_ACCESS_DENIED = "B1017";
	public static final String ERR_DESC_ACCESS_DENIED = "Access denied";
	public static final String ERR_CODE_GROUP_STARTED = "B1018";
	public static final String ERR_DESC_GROUP_STARTED = "The group is started";
	public static final String ERR_CODE_GROUP_FINISHED = "B1019";
	public static final String ERR_DESC_GROUP_FINISHED = "The group is finished";
	public static final String ERR_CODE_INVD_OLD_PASSWORD = "B1020";
	public static final String ERR_DESC_INVD_OLD_PASSWORD = "Invalid old password";
	public static final String ERR_CODE_CAN_NOT_START_RANDOM = "B1021";
	public static final String ERR_DESC_CAN_NOT_START_RANDOM = "Can't start random";
	public static final String ERR_CODE_CAN_NOT_FINISH_RANDOM = "B1022";
	public static final String ERR_DESC_CAN_NOT_FINISH_RANDOM = "Can't finish random";
	public static final String ERR_CODE_USER_ALREADY_REQUEST = "B1023";
	public static final String ERR_DESC_USER_ALREADY_REQUEST = "User already in Request Event Group";
	public static final String ERR_CODE_FIRST_LAST_NAME_DUPLICATE = "B1024";
	public static final String ERR_DESC_FIRST_LAST_NAME_DUPLICATE = "Firstname and Lastname Duplicate";
	
	public static final String ERR_CODE_CAPTCHA_ERROR = "B1025";
	public static final String ERR_DESC_CAPTCHA_ERROR = "Captcha have problem";
	
	public static final String ERR_CODE_EMAIL_NOT_FOUND = "B1026";
	public static final String ERR_DESC_EMAIL_NOT_FOUND = "Account with that email address not found";
	public static final String ERR_CODE_REGISTER_ALREADY = "B1027";
	public static final String ERR_DESC_REGISTER_ALREADY = "You register already";
	public static final String ERR_CODE_USER_FULL_NAME_DUPLICATE = "B1028";
	public static final String ERR_DESC_USER_FULL_NAME_DUPLICATE = "User Full Name is Duplicate";

}
