package com.xplink.api.config.sql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

/**
 * <p> XPLink Framework </p>
 * @version 1.0
 * @author Phongsathorn Angyarn <phongsathorn@xp-link.com>
 * @since December, 2016
 *
 */

@Configuration
@PropertySource("classpath:config.sql.xml")
public class SQLPropertyFileConfig {

	@Autowired
	private Environment env;
	
	public String getSql(String key) {
		return env.getProperty(key);
	}
	
	public static String removeNewLineAndTab(final String str) {
		return str.replaceAll("\n|\t", " ");
	}
}
