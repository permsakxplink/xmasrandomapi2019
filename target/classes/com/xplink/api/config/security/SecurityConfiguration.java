package com.xplink.api.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	@Qualifier("customUserDetailsService")
	UserDetailsService userDetailsService;

	@Autowired
	PersistentTokenRepository tokenRepository;

	@Autowired
	public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService);
		auth.authenticationProvider(authenticationProvider());
//		auth.inMemoryAuthentication()
//		.withUser("admin")
//		.password("password")
//		.authorities("ROLE_ADMIN");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
//		http.authorizeRequests()
//		.antMatchers( "/**") .permitAll()
//        .anyRequest().authenticated()
//        .and()
//		.formLogin().loginPage("/authen").loginProcessingUrl("/authen").usernameParameter("username").passwordParameter("password")
//		.and()
//		.rememberMe().rememberMeParameter("remember-me").tokenRepository(tokenRepository)
//		.tokenValiditySeconds(86400)
//		.and()
//		.csrf()
//        .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
//		.and()
//		.exceptionHandling().accessDeniedPage("/Access_Denied");
		
//		http
//	      .authorizeUrls()
//	        .antMatchers("/signup","/about").permitAll() // #4
//	        .antMatchers("/admin/**").hasRole("ADMIN") // #6
//	        .anyRequest().authenticated() // 7
//	        .and()
//	    .formLogin()  // #8
//	        .loginUrl("/login") // #9
//	        .permitAll(); // #5
		
		System.out.println(">>>>>>>> HttpSecurity configuration");		
		http.authorizeRequests()
		.anyRequest().permitAll()
		.and()
		.csrf().disable();
		
//		System.out.println(">>>>>>>>");
//		http.authorizeRequests()
//	      .antMatchers("/**")
//	      .permitAll();
		
//		http
//		.csrf()
//		.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
		
//		http.csrf().disable();
//	    http.authorizeRequests().antMatchers("/authen").permitAll()
//        .antMatchers("/**").access("isAuthenticated()")
//        .and()
//        .formLogin()
//        .loginProcessingUrl("/authen")
//        .loginPage("/authen")
//        //.successHandler(successHandler)
//        //.failureHandler(failureHandler).defaultSuccessUrl("/pages/dashboard.xhtml")
//        .usernameParameter("username").passwordParameter("password")
//        .and().sessionManagement().maximumSessions(2)
//        .maxSessionsPreventsLogin(true);
	    
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
		authenticationProvider.setUserDetailsService(userDetailsService);
		authenticationProvider.setPasswordEncoder(passwordEncoder());
		return authenticationProvider;
	}

	@Bean
	public PersistentTokenBasedRememberMeServices getPersistentTokenBasedRememberMeServices() {
		return new PersistentTokenBasedRememberMeServices("remember-me", userDetailsService, tokenRepository);
	}

	@Bean
	public AuthenticationTrustResolver getAuthenticationTrustResolver() {
		return new AuthenticationTrustResolverImpl();
	}
	
}
