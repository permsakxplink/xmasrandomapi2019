package com.xplink.api.xmas.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;
import com.xplink.api.exception.ServiceException;
import com.xplink.api.log.AbstractLogManager;
import com.xplink.api.rest.constant.Constant;
import com.xplink.api.rest.constant.ErrorConstant;
import com.xplink.api.rest.controller.vo.GenericRestResponse;
import com.xplink.api.rest.controller.vo.RestHeaderRequest;
import com.xplink.api.xmas.controller.vo.EmailInfo;
import com.xplink.api.xmas.controller.vo.UserInfo;
import com.xplink.api.xmas.dao.AuthorizeDao;
import com.xplink.api.xmas.dao.EventDao;
import com.xplink.api.xmas.dao.GroupDao;
import com.xplink.api.xmas.dao.UserDao;
import com.xplink.api.xmas.dao.vo.Event;
import com.xplink.api.xmas.dao.vo.Group;
import com.xplink.api.xmas.dao.vo.PersistentLogin;
import com.xplink.api.xmas.dao.vo.User;
import com.xplink.api.xmas.service.concurrent.task.MailSenderTask;

@Service
public class EmailService extends AbstractLogManager {

	@Autowired
	private Gson gson;
	
	@Autowired
	AuthorizeDao authorizeDao;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private GroupDao groupDao;
	
	@Autowired
	private EventDao eventDao;

	@Autowired
	private JavaMailSender mailSender;

	@Value("${mail.xmas.from}")
	private String mailFrom;

	@Value("${mail.xmas.subject}")
	private String mailSubject;
	
	@Value("${mail.xmas.subject.forget}")
	private String mailSubjectForget;
	
	@Value("${app.conf.dir}")
	private String confDir;
	
	@Autowired
	private ThreadPoolExecutor executor;
	
	static final String key = "1234567890abcdefghijklmnopqrstuvwxyz";
	static Random random = new Random();
	
	private String generatePassword() {
		
		StringBuilder strBuild = new StringBuilder();
		for (int i = 0; i < 10; i++) {
			strBuild.append(key.charAt( random.nextInt(key.length())));
		}
		
		return strBuild.toString();
	}
	
	private void sendEmail(String mailTo, String message, String subject) {
		String uuid = ThreadContext.get(Constant.LOG_PARM_KEY_UUID);
		String method = ThreadContext.get(Constant.LOG_PARM_KEY_HTTP_METHOD);
		String bizService = ThreadContext.get(Constant.LOG_PARM_KEY_BIZ_SERVICE);
		
		MailSenderTask task = new MailSenderTask(this.mailSender, this.mailFrom, mailTo, subject, message, uuid, bizService, method);
		this.executor.execute(task);

		try {
			logger.info("Sleep 2000 ms. before sending email");
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			logger.error("Cannot sleep before sending email, error=" + e.getMessage());
		}
	}

	public String inviteWithEmailFromGroup(String email, String groupName, RestHeaderRequest header) throws UnknownHostException {
		log_info("Send email Invite");
		
		PersistentLogin persist = this.authorizeDao.findRoleAdmin(header.getToken());
		User user = this.userDao.findById(persist.getUserId());
		
		String mailTemplate = readMailTemplate("/invite.template");
		String dataLink = new String(Base64Utils.encode(("biz=invite,username="+email+",group_name="+groupName+",user_invite="+user.getUsername()).getBytes()));
		String link = "<a href='https://35.240.172.10:8443/email?data=" +dataLink+ "'>Click</a>";
		log_info("HostAddress=" +header.getOrigin());
		String message = mailTemplate.replace("${user-invite}", user.getFirstName()+" "+user.getLastName());
		message = message.replace("${group-name}", groupName);
		message = message.replace("${accept-Link}", link);
		
		sendEmail(email, message, this.mailSubject.replace("[XMas]", "[XMas-Invite group]"));

		return this.gson.toJson(new GenericRestResponse<String>());
	}
	
	public String responseInviteWithEmail(RestHeaderRequest header, EmailInfo emailInfo) throws UnknownHostException, ServiceException {
		log_info("Send email response accept Invite");
		
		if ( StringUtils.isEmpty(emailInfo.getUsername()) || StringUtils.isEmpty(emailInfo.getGroupName()) || StringUtils.isEmpty(emailInfo.getUsernameInvite())) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_UNEXPECTED, ErrorConstant.ERR_DESC_UNEXPECTED);
		}
		
		User user = this.userDao.findByUsername(emailInfo.getUsername());
		if ( user != null ) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_REGISTER_ALREADY, ErrorConstant.ERR_DESC_REGISTER_ALREADY);
		}
		
		user = this.userDao.findByUsername(emailInfo.getUsernameInvite());
		String mailTemplate = readMailTemplate("/responseInvite.template");
		String password = generatePassword();
//		String link = "<a href='http://localhost:3000/#!/signin'>Click</a>";
		String link = "<a href='https://christmas-2ba45.firebaseapp.com/#/'>Click</a>";
		log_info("HostAddress=" +header.getRequestURL());
		String message = mailTemplate.replace("${username}", emailInfo.getUsername());
		message = message.replace("${password}", password);
		message = message.replace("${group-name}", emailInfo.getGroupName());
		message = message.replace("${user-invite}", user.getFirstName()+ " " +user.getLastName());
		message = message.replace("${status-time}", formatTime(Calendar.getInstance(Locale.US).getTime()));
		message = message.replace("${SignIn-Link}", link);
		
		String[] paramArray = emailInfo.getUsername().split("@");
		String firstName = paramArray[0];

		List<User> userList= this.userDao.findAllUsers();
		int count = 1;
		log_info("userList"+userList);
		String lastName = null;
		lastName = firstName+"_"+count;
		for (User usr : userList) {
			lastName = firstName+"_"+count;
			count = usr.getLastName().equals(lastName) ? count++ : count;
		}	
		log_info("lastName" + lastName);
		user = new User();
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setUsername(emailInfo.getUsername());
		user.setPassword(new String(Base64Utils.encode(password.getBytes())));
		user.setLastChngDttm(Calendar.getInstance(Locale.US).getTime());
		int id = this.userDao.create(user);
		user.setUserId(id);
		
		Group group = this.groupDao.findByGroupName(emailInfo.getGroupName());
		if ( group == null ) {
			message = message.replace("${status-room}", "This room isn't found");
		} else {
			if ( group.getGroupState() == 0 ) {
				message = message.replace("${status-room}", "Ready");
			} else if ( group.getGroupState() == 1 ) {
				message = message.replace("${status-room}", "Started");
			} else if ( group.getGroupState() == 2 ) {
				message = message.replace("${status-room}", "Finished");
			}
			Event event = this.eventDao.findByGroupId(group.getGroupId(), user.getUserId());
			if ( event == null && group.getGroupState() == 0 ) {
				event = new Event();
				event.setUserStts(0);
				event.setUserId(user.getUserId());
				event.setGroupId(group.getGroupId());
				event.setEvtLastChngDttm(Calendar.getInstance(Locale.US).getTime());
				this.eventDao.create(event);
			} else {

			}
		}
		
		log_debug("message="+message);
		sendEmail(emailInfo.getUsername(), message, this.mailSubject.replace("[XMas]", "[XMas-Invite & Register]"));
		
		String userRequestName = user.getFirstName()+ " " +user.getLastName();
		user = this.userDao.findById(group.getGroupCreatedBy());
		requestEmailToHeadRoom(header, user, userRequestName, group.getGroupName());
		
		return this.gson.toJson(new GenericRestResponse<String>());
	}
	
	public String acceptRegister(UserInfo userInfo, RestHeaderRequest header) throws UnknownHostException {
		log_info("Send email accept register");
		String mailTemplate = readMailTemplate("/acceptregister.template");
		String dataLink = new String(Base64Utils.encode(("biz=create,username="+userInfo.getUsername()+",password="+userInfo.getPassword()+",first_name="+userInfo.getFirstName()+",last_name="+userInfo.getLastName()).getBytes()));
//		String link = "<a href='" +header.getOrigin()+ ":80/email?data=" +dataLink+ "'>Click</a>";
		String link = "<a href='https://35.240.172.10:8443/email?data=" +dataLink+ "'>Click</a>";
		log_info("HostAddress=" +header.getOrigin());
		log_info("getRequestURL=" +header.getRequestURL());
		String message = mailTemplate.replace("${fullname}", userInfo.getFirstName()+" "+userInfo.getLastName());
		message = message.replace("${accept-Link}", link);
		
		sendEmail(userInfo.getUsername(), message, this.mailSubject.replace("[XMas]", "[XMas-Invite & Register]"));
		
		return this.gson.toJson(new GenericRestResponse<String>());
	}
	
	public String forgetPasswordEmail(RestHeaderRequest header, User user) {
		log_info("Send email accept register");
		String mailTemplate = readMailTemplate("/forgetpassword.template");
		String password = generatePassword();
		String link = "<a href='https://christmas-2ba45.firebaseapp.com/#/'>Click</a>";
		log_info("HostAddress=" +header.getOrigin());
		String message = mailTemplate.replace("${fullname}", user.getFirstName()+" "+user.getLastName());
		message = message.replace("${username}", user.getUsername());
		message = message.replace("${password}", password);
		message = message.replace("${SignIn-Link}", link);
		
		user.setPassword(new String(Base64Utils.encode(password.getBytes())));
		this.userDao.update(user);
		
		sendEmail(user.getUsername(), message, this.mailSubjectForget);
		
		return this.gson.toJson(new GenericRestResponse<String>());
	}
	
	public String requestEmailToHeadRoom(RestHeaderRequest header,User createBy, String userRequest, String groupName) {
		log_info("Send email request");
		String mailTemplate = readMailTemplate("/request.template");
		String link = "<a href='https://christmas-2ba45.firebaseapp.com/#/'>Click</a>";
		log_info("HostAddress=" +header.getOrigin());
		String message = mailTemplate.replace("${user-request}", userRequest);
		log_debug(">>>>>>>>>>>>>>>>>>.createBy=" + gson.toJson(createBy));
		message = message.replace("${user-create}", createBy.getFirstName()+ " " + createBy.getLastName());
		message = message.replace("${group-name}", groupName);
		message = message.replace("${link-signin}", link);
			
		sendEmail(createBy.getUsername(), message, this.mailSubject.replace("[XMas]", "[XMas-Request]"));
		
		return this.gson.toJson(new GenericRestResponse<String>());
	}
	
	public String inviteRegister(RestHeaderRequest header,UserInfo userInfo) {
		log_info("Send email invite register");
		String mailTemplate = readMailTemplate("/inviteregister.template");
		String link = "<a href='https://christmas-2ba45.firebaseapp.com/#/'>SignUp</a>";
		log_info("HostAddress=" +header.getOrigin());
		String message = mailTemplate.replace("${email}", userInfo.getUsername());
		message = message.replace("${signUp-link}", link);
			
		sendEmail(userInfo.getUsername(), message, this.mailSubject.replace("[XMas]", "[XMas-Invite & Register]"));
		
		return this.gson.toJson(new GenericRestResponse<String>());
	}
	
	private String readMailTemplate(String path) {
	//	String filePath = System.getProperty("user.home") + File.separator + confDir + File.separator + "invite.template";
	//	log_debug("Mail Template Url={0}", filePath);
		
		InputStream in = getClass().getResourceAsStream(path); 
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
			StringBuilder builder = new StringBuilder();
			while (true) {
				String line = reader.readLine();
				if (line == null) {
					break;
				}

				if ("".equals(line.trim())) {
					continue;
				}

				builder.append(line);
			}

			return builder.toString();
		} catch (Throwable e) {
			log_error("Error ReadMailTemplate :: " ,e);
			return "";
		}
	}
	
	public String formatTime(Date date) {
		SimpleDateFormat formatTime = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
		return formatTime.format(date);
	}
}
