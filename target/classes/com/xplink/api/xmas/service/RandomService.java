package com.xplink.api.xmas.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;

import com.google.gson.Gson;
import com.xplink.api.exception.ServiceException;
import com.xplink.api.log.AbstractLogManager;
import com.xplink.api.rest.constant.Constant;
import com.xplink.api.rest.constant.ErrorConstant;
import com.xplink.api.rest.controller.vo.GenericRestResponse;
import com.xplink.api.rest.controller.vo.RestHeaderRequest;
import com.xplink.api.xmas.controller.vo.EventInfo;
import com.xplink.api.xmas.controller.vo.GroupInfo;
import com.xplink.api.xmas.controller.vo.RandomInfo;
import com.xplink.api.xmas.dao.AuthorizeDao;
import com.xplink.api.xmas.dao.EventDao;
import com.xplink.api.xmas.dao.GroupDao;
import com.xplink.api.xmas.dao.UserDao;
import com.xplink.api.xmas.dao.vo.Event;
import com.xplink.api.xmas.dao.vo.Group;
import com.xplink.api.xmas.dao.vo.PersistentLogin;
import com.xplink.api.xmas.dao.vo.User;
import com.xplink.api.xmas.service.concurrent.task.MailSenderTask;
import com.xplink.api.xmas.util.KeywordRandomUtil;

@Service
public class RandomService extends AbstractLogManager {

	@Autowired
	private Gson gson;
	
	@Autowired
	GroupDao groupDao;
	
	@Autowired
	AuthorizeDao authorizeDao;

	@Autowired
	GroupService groupService;

	@Autowired
	private UserDao userDao;

	@Autowired
	private EventDao eventDao;

	@Value("${mail.xmas.from}")
	private String mailFrom;

	@Value("${mail.xmas.subject}")
	private String mailSubject;

	@Value("${app.conf.dir}")
	private String confDir;

	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private ThreadPoolExecutor executor;

	public String random(RestHeaderRequest header, String groupName) throws ServiceException {
		log_debug("call >>> RandomService.random()");
		Group group = this.groupDao.findByGroupName(groupName);
		GroupInfo groupInfo = new GroupInfo();
		groupInfo.setGroupId(group.getGroupId());
		List<Event> list = this.eventDao.findEventByGroup(groupInfo);
		PersistentLogin persist = this.authorizeDao.findRoleAdmin(header.getToken());
		if ( group.getGroupCreatedBy() != persist.getUserId() ) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_ACCESS_DENIED, ErrorConstant.ERR_DESC_ACCESS_DENIED);
		}
		
		String keywordCreator = this.eventDao.findByGroupId(group.getGroupId(), group.getGroupCreatedBy()).getEvtKeyword();
		if ( keywordCreator == null ) {
			log_info("Keyword of Create by is null.");
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_CAN_NOT_START_RANDOM, ErrorConstant.ERR_DESC_CAN_NOT_START_RANDOM);			
		}
		
		int countKeyword = 0;
		for ( Event event : list ) {
			countKeyword = event.getEvtKeyword() != null ? countKeyword+1 : countKeyword;
		}
		log_debug("Check Players < 3 persons :: = " +countKeyword);
		if ( countKeyword < 3 ) {
			log_info("Players input keyword < 3");
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_CAN_NOT_START_RANDOM, ErrorConstant.ERR_DESC_CAN_NOT_START_RANDOM);			
		}
		
		EventInfo eventInfo = new EventInfo();
		for ( Event event : list ) {
			log_debug("Check delete :: event.getUserId() = " +event.getUserId()+", event.getEvtKeyword() = " +event.getEvtKeyword());
			if ( event.getUserStts() == 0 || event.getEvtKeyword() == null ) {
				eventInfo.setGroupId(event.getGroupId());
				eventInfo.setUserId(event.getUserId());
				this.eventDao.deleteByUserId(eventInfo);
			}
		}

		list = this.eventDao.findEventByGroup(groupInfo);
		for (Event event : list) {
			log_debug("Check status :: event.getUserId() = " +event.getUserId()+", event.getEvtKeyword() = " +event.getEvtKeyword());
			log_debug("group.getGroupState() = " +group.getGroupState());
			log_debug("event.getEvtKeyword() = " +event.getEvtKeyword());
			if ( event.getEvtKeyword() == null || group.getGroupState() != 0 ) {
				throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_CAN_NOT_START_RANDOM, ErrorConstant.ERR_DESC_CAN_NOT_START_RANDOM);
			}
		}

		List<Event> eventList = new ArrayList<Event>();
		List<String> keywordList = new ArrayList<String>();
		for (Event evnt : list) {
			eventList.add(evnt);
			keywordList.add(evnt.getEvtKeyword());
		}

		List<String> randomKeywordList = KeywordRandomUtil.random(keywordList, this.logger);
		String mailTemplate = readMailTemplate();
		String uuid = ThreadContext.get(Constant.LOG_PARM_KEY_UUID);
		String bizService = ThreadContext.get(Constant.LOG_PARM_KEY_BIZ_SERVICE);
		String method = ThreadContext.get(Constant.LOG_PARM_KEY_HTTP_METHOD);
		log_debug("Template=" + mailTemplate);

		if ("event-start".equals(header.getBizService())) {
			int size = eventList.size();
			log_debug("start random keyword");
			for (int i = 0; i < size; i++) {
				Event event = eventList.get(i);
				User user = userDao.findById(event.getUserId());
				String email = user.getUsername();
				String link = "<a href='christmas-2ba45.firebaseapp.com'>Click</a>";
				String randomKeyword = randomKeywordList.get(i);
				String message = mailTemplate.replace("${RandomKeyword}", randomKeyword == null ? null : new String(Base64Utils.decode(randomKeyword.getBytes())));
				message = message.replace("${link-signin}", link);
				eventInfo = new EventInfo();
				eventInfo.setUserId(event.getUserId());
				eventInfo.setGroupId(group.getGroupId());

				Event evt = eventDao.findById(event.getEvtId()); 
				evt.setEvtReceiveKeyword(randomKeyword);
				eventDao.update(evt);
				log_debug("userId-"+ event.getUserId() +"Update receive keyword=" + evt.getEvtReceiveKeyword());

				MailSenderTask task = new MailSenderTask(this.mailSender, this.mailFrom, email, this.mailSubject.replace("[XMas]", "[XMas-Keyword]"),
						message, uuid, bizService, method);
				log_debug("MailSenderTask = " + task);
				this.executor.execute(task);

				try {
					logger.info("Sleep 2000 ms. before sending email");
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					logger.error("Cannot sleep before sending email, error=" + e.getMessage());
				}
				log_debug("random process : " + (i+1));
			}
			log_debug("finish random keyword");
			
		}
		
		groupInfo.setGroupState(Constant.GROUP_STATE_START);
		this.groupDao.updateState(group.getGroupId(), groupInfo.getGroupState());
		
		return this.gson.toJson(new GenericRestResponse<String>());

	}
	
	public String randomFinish(String groupName, String token) throws ServiceException {
		PersistentLogin persist = this.authorizeDao.findRoleAdmin(token);
		Group group = this.groupDao.findByGroupName(groupName);
		
		if (persist.getUserId() != group.getGroupCreatedBy()) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_ACCESS_DENIED, ErrorConstant.ERR_CODE_ACCESS_DENIED);
		}
		if (group.getGroupState() != 1) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_CAN_NOT_FINISH_RANDOM, ErrorConstant.ERR_DESC_CAN_NOT_FINISH_RANDOM);
		}
		
		group.setGroupState(Constant.GROUP_STATE_FINISH);
		groupService.update(group, persist.getUserId());
		
		List<Event> list = this.eventDao.findByGroupId(group.getGroupId());
		List<RandomInfo> randomList = new ArrayList<RandomInfo>();
		for ( Event event : list ) {
			RandomInfo randomInfo = new RandomInfo();
			User user = this.userDao.findById(event.getUserId());
			randomInfo.setPlayer(user.getUsername());
			randomInfo.setPlayer_keyword(event.getEvtKeyword());
			randomInfo.setProvider_keyword(event.getEvtReceiveKeyword());
			Event evn = this.eventDao.findByKeyword(group.getGroupId(), event.getEvtReceiveKeyword());
			user = this.userDao.findById(evn.getUserId());
			randomInfo.setProvider(user.getUsername());
			randomList.add(randomInfo);
		}
		
		GenericRestResponse<List<RandomInfo>> response = new GenericRestResponse<List<RandomInfo>>();
		response.setData(randomList);
		return this.gson.toJson(response);
	}

	private String readMailTemplate() {		
		InputStream in = getClass().getResourceAsStream("/mail.template");
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
			StringBuilder builder = new StringBuilder();
			while (true) {
				String line = reader.readLine();
				if (line == null) {
					break;
				}

				if ("".equals(line.trim())) {
					continue;
				}

				builder.append(line);
			}

			return builder.toString();
		} catch (Throwable e) {
			return "";
		}
	}
	/*
	 * public String currentRandom(String biz) throws ServiceException { Random
	 * rand = this.randDao.findCurrentRandom(); RandomInfo randInfo = new
	 * RandomInfo(); if (rand == null && "random".equals(biz)) { // throw new
	 * ServiceException(this.getClass().getSimpleName(), //
	 * ErrorConstant.ERR_CODE_RANDOM_NOT_FOUND, //
	 * ErrorConstant.ERR_DESC_RANDOM_NOT_FOUND); Random newRand = new Random();
	 * newRand.setRandDttm(Calendar.getInstance(Locale.US).getTime());
	 * newRand.setRandState(1); int id = this.randDao.create(newRand);
	 * 
	 * List<User> users = this.userDao.findAllUsers();
	 * 
	 * for (User usr : users) { int kywdId =
	 * keywordDao.findByUsername(usr.getUsername()).getKeywordId(); Event evt =
	 * new Event(); evt.setEvtRandId(id); evt.setEvtUsername(usr.getUsername());
	 * //evt.setEvtKeywordDesc(kywdId); evtDao.create(evt); }
	 * 
	 * newRand.setRandId(id); randInfo.setRandId(newRand.getRandId());
	 * 
	 * GenericRestResponse<RandomInfo> response = new
	 * GenericRestResponse<RandomInfo>(); response.setData(randInfo); return
	 * this.gson.toJson(response); }
	 * 
	 * randInfo.setRandDttm(rand.getRandDttm());
	 * randInfo.setRandState(rand.getRandState());
	 * 
	 * GenericRestResponse<RandomInfo> response = new
	 * GenericRestResponse<RandomInfo>(); response.setData(randInfo); return
	 * this.gson.toJson(response); 
	 * }
	 */
}
