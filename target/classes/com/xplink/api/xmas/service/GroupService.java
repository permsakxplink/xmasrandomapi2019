package com.xplink.api.xmas.service;

import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;
import com.xplink.api.exception.ServiceException;
import com.xplink.api.log.AbstractLogManager;
import com.xplink.api.rest.constant.Constant;
import com.xplink.api.rest.constant.ErrorConstant;
import com.xplink.api.rest.controller.vo.GenericRestResponse;
import com.xplink.api.rest.controller.vo.RestHeaderRequest;
import com.xplink.api.xmas.controller.vo.EventInfo;
import com.xplink.api.xmas.controller.vo.GroupAllReqs;
import com.xplink.api.xmas.controller.vo.GroupAllRespParam;
import com.xplink.api.xmas.controller.vo.GroupInfo;
import com.xplink.api.xmas.dao.AuthorizeDao;
import com.xplink.api.xmas.dao.EventDao;
import com.xplink.api.xmas.dao.GroupDao;
import com.xplink.api.xmas.dao.UserDao;
import com.xplink.api.xmas.dao.vo.Event;
import com.xplink.api.xmas.dao.vo.Group;
import com.xplink.api.xmas.dao.vo.PersistentLogin;
import com.xplink.api.xmas.dao.vo.User;

@Service
public class GroupService extends AbstractLogManager {

	@Autowired
	private Gson gson;

	@Autowired
	GroupDao groupDao;

	@Autowired
	UserDao userDao;
	
	@Autowired
	EventDao eventDao;
	
	@Autowired
	AuthorizeDao authorizeDao;
	
	@Autowired
	private EmailService emailService;
	
	public String create(String groupName, String token) throws ServiceException {
		Group group = this.groupDao.findByGroupName(groupName);
		if (group != null) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_GROUP_NAME_DUPLICATE, ErrorConstant.ERR_DESC_GROUP_NAME_DUPLICATE);
		}
		
		PersistentLogin persist = this.authorizeDao.findRoleAdmin(token);
		if (persist != null && "ADMIN".equals(persist.getRoleName())) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_ADMIN_CANNOT_CREATE, ErrorConstant.ERR_DESC_ADMIN_CANNOT_CREATE);
		}
		
		group = new Group();
		group.setGroupDttm(Calendar.getInstance(Locale.US).getTime());
		group.setGroupCreatedBy(persist.getUserId());
		group.setGroupName(groupName);
		group.setGroupState(0);
		
		int id = this.groupDao.create(group);
		GroupInfo groupInfo = new GroupInfo();
		groupInfo.setGroupId(id);
		groupInfo.setGroupName(groupName);

		Event evt = new Event();
		evt.setUserStts(1);
		evt.setGroupId(groupInfo.getGroupId());
		evt.setUserId(group.getGroupCreatedBy());
		evt.setEvtLastChngDttm(Calendar.getInstance(Locale.US).getTime());
		eventDao.create(evt);
		
		int players = this.eventDao.findPlayersByGroup(groupInfo.getGroupId()).size();
		groupInfo.setPlayers(players);

		GenericRestResponse<GroupInfo> response = new GenericRestResponse<GroupInfo>();
		response.setData(groupInfo);
		return this.gson.toJson(response);
	}
	
	public String joinGroup(String groupName, Integer userId, String token) throws ServiceException {
		log_debug("#############################");
		log_debug("User ID=" + userId);
		log_debug("Group Name =" + groupName);
		Group group = this.groupDao.findByGroupName(groupName);
		Event evt = new Event();
		evt.setUserStts(1);
		evt.setGroupId(group.getGroupId());
		evt.setUserId(userId);
		evt.setEvtLastChngDttm(Calendar.getInstance(Locale.US).getTime());
		eventDao.create(evt);
		return null;
	}

	public void update(Group group, Integer userId) throws ServiceException {
		if(userId == group.getGroupCreatedBy()){
			groupDao.update(group);
		}else{
			log_info("You have been denied permission to access group");
		}
	}
	
	public void validationGroupStatus(Group group) throws ServiceException {
		if (group == null) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_GROUP_NOT_FOUND, ErrorConstant.ERR_DESC_GROUP_NOT_FOUND);
		}
		if (group.getGroupState() == 1) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_GROUP_STARTED, ErrorConstant.ERR_DESC_GROUP_STARTED);
		}
		if (group.getGroupState() == 2) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_GROUP_FINISHED, ErrorConstant.ERR_DESC_GROUP_FINISHED);
		}
	}
	
	public String requestGroup(String inviteEmail, String groupName, RestHeaderRequest header) throws ServiceException {		
		PersistentLogin persist = this.authorizeDao.findRoleAdmin(header.getToken());
		Group group = this.groupDao.findByGroupName(groupName);
		validationGroupStatus(group);
		User user = this.userDao.findById(persist.getUserId());
		
		Event evt = new Event();
		if (inviteEmail == null) {
			evt.setUserId(persist.getUserId());
		} else {
			Event event = this.eventDao.findByGroupId(group.getGroupId(), persist.getUserId());
			if ( event == null ) {
				throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_ACCESS_DENIED, ErrorConstant.ERR_DESC_ACCESS_DENIED);
			} else if ( event.getUserStts() != 1 ) {
				throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_ACCESS_DENIED, ErrorConstant.ERR_DESC_ACCESS_DENIED);
			}
			
			user = this.userDao.findByUsername(inviteEmail);
			if ( user == null ) {
				try {
					return this.emailService.inviteWithEmailFromGroup(inviteEmail, groupName, header);
				} catch (NumberFormatException | UnknownHostException e) {
					log_info(e.getMessage());
				}
			} else if ( user.getUserId() == persist.getUserId() ) {
				throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_USER_DUPLICATE, ErrorConstant.ERR_DESC_USER_DUPLICATE);
			}

			event = this.eventDao.findByGroupId(group.getGroupId(), user.getUserId());
			if ( event == null ) {
				evt.setUserId(user.getUserId());
			}else if ( event.getUserStts() == 0 ) {
				throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_USER_ALREADY_REQUEST, ErrorConstant.ERR_DESC_USER_ALREADY_REQUEST);
			} else {
				evt.setUserId(user.getUserId());
			}
		}
		
		evt.setUserStts(0);
		evt.setGroupId(group.getGroupId());
		evt.setEvtLastChngDttm(Calendar.getInstance(Locale.US).getTime());
		this.eventDao.create(evt);
		
		sendEmailToHeadRoom(header, group.getGroupId(), evt.getUserId());
		
		return this.gson.toJson(new GenericRestResponse<String>());
	}
	
	private void sendEmailToHeadRoom(RestHeaderRequest header, Integer groupId, Integer userRequestId) throws ServiceException {
		log_info("Send email to Head Room");
		Group group = this.groupDao.findById(groupId);
		validationGroupStatus(group);
		User user = this.userDao.findById(userRequestId);
		String userRequestName = user.getFirstName()+ " " +user.getLastName();
		
		user = this.userDao.findById(group.getGroupCreatedBy());
		
		try {
			this.emailService.requestEmailToHeadRoom(header, user, userRequestName, group.getGroupName());
		} catch (NumberFormatException e) {
			log_info(e.getMessage());
		}
	}
	
	public String acceptGroup(String accept, String groupName, Integer userId, String token) throws ServiceException {
		PersistentLogin persist = this.authorizeDao.findRoleAdmin(token);
		Group group = this.groupDao.findByGroupName(groupName);
		
		if ( group == null ) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_GROUP_NOT_FOUND, ErrorConstant.ERR_DESC_GROUP_NOT_FOUND);
		}else if (group.getGroupCreatedBy() != persist.getUserId()) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_ACCESS_DENIED, ErrorConstant.ERR_DESC_ACCESS_DENIED);
		}
		
		Event event = this.eventDao.findByGroupId(group.getGroupId(), userId);
		if ( event == null ) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_USER_NOT_FOUND, ErrorConstant.ERR_DESC_USER_NOT_FOUND);
		}
		
		EventInfo eventInfo = new EventInfo();
		if ( "accept".equals(accept)) {
			eventInfo.setUserStts(1);
			eventInfo.setUserId(event.getUserId());
			eventInfo.setGroupId(event.getGroupId());
			eventInfo.setLastChngDttm(Calendar.getInstance(Locale.US).getTime());
			this.eventDao.updateSttsUser(eventInfo);
		} else if ( "delete".equals(accept) ) {
			eventInfo.setUserId(event.getUserId());
			eventInfo.setGroupId(event.getGroupId());
			this.eventDao.deleteByUserId(eventInfo);
		}	
		
		return this.gson.toJson(new GenericRestResponse<String>());
	}
	
	public String acceptGroup(String accept, String groupName, String token) throws ServiceException {
		PersistentLogin persist = this.authorizeDao.findRoleAdmin(token);
		Group group = this.groupDao.findByGroupName(groupName);
		
		if ( group == null ) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_GROUP_NOT_FOUND, ErrorConstant.ERR_DESC_GROUP_NOT_FOUND);
		}else if (group.getGroupCreatedBy() != persist.getUserId()) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_ACCESS_DENIED, ErrorConstant.ERR_DESC_ACCESS_DENIED);
		}
		
		List<Event> list = this.eventDao.findByGroupId(group.getGroupId());
		EventInfo eventInfo = new EventInfo();
		for ( Event event : list )
			if ( "accept-all".equals(accept) && event.getUserStts() == 0 ) {
				eventInfo.setGroupId(event.getGroupId());
				eventInfo.setUserId(event.getUserId());
				eventInfo.setLastChngDttm(Calendar.getInstance(Locale.US).getTime());
				eventInfo.setUserStts(1);
				this.eventDao.updateSttsUser(eventInfo);
			
			} else if ( "delete-all".equals(accept) && event.getUserStts() == 0 ) {
				eventInfo.setGroupId(event.getGroupId());
				eventInfo.setUserId(event.getUserId());
				this.eventDao.deleteByUserId(eventInfo);
			}	

		return this.gson.toJson(new GenericRestResponse<String>());
	}

	public String findById(Integer groupId) throws ServiceException {
		Group group = this.groupDao.findById(groupId);
		if (group == null) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_GROUP_NOT_FOUND, ErrorConstant.ERR_DESC_GROUP_NOT_FOUND);
		}

		GroupInfo groupInfo = new GroupInfo();
		groupInfo.setGroupId(group.getGroupId());
		groupInfo.setGroupName(group.getGroupName());
		groupInfo.setGroupDttm(group.getGroupDttm());
		groupInfo.setGroupState(group.getGroupState());
		groupInfo.setGroupCreatedBy(group.getGroupCreatedBy());
		groupInfo.setPlayers(1);
		GenericRestResponse<GroupInfo> response = new GenericRestResponse<GroupInfo>();
		response.setData(groupInfo);
		return this.gson.toJson(response);
	}

	public String findByLimit(GroupAllReqs request, String token, String bizservice) throws ServiceException {
		log_debug("SortBy="+request.getSortBy()+" SortField="+request.getSortField()+" Search="+request.getSearch());
		PersistentLogin persist = this.authorizeDao.findRoleAdmin(token);
		
		request.setSearch( StringUtils.isEmpty( request.getSearch() ) ? "" : request.getSearch());
		request.setSortBy( StringUtils.isEmpty( request.getSortBy() ) ? "ASC" : request.getSortBy() );
		request.setSortField( StringUtils.isEmpty( request.getSortField() ) ? "g.grp_dttm" : request.getSortField() );
		
		if ("all-group".equals(bizservice)) {
			request.setWhereField("");
		} else if ("all-group-objects".equals(bizservice)) {
			request.setWhereField("g.grp_createdby = " +persist.getUserId()+ " AND");
		} else if ("all-group-acceptance".equals(bizservice)) {
			request.setWhereField("e.xmas_user_stts = 0 AND e.xmas_user_usr_id = " +persist.getUserId()+ " AND");
		} else if ("all-group-membership".equals(bizservice)) {
			request.setWhereField("NOT (g.grp_createdby = " +persist.getUserId()+ " OR e.xmas_user_stts = 0) AND e.xmas_user_usr_id = " +persist.getUserId()+ " AND");
		}
		
		List<Group> list = this.groupDao.findByLimit(request);		
		if (list == null) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_GROUP_NOT_FOUND, ErrorConstant.ERR_DESC_GROUP_NOT_FOUND);
		}
		
		List<GroupAllRespParam> xmasResponseList = new ArrayList<GroupAllRespParam>();
		int countNo = request.getOffset();
		for (Group group : list ) {
			GroupAllRespParam xmasInfo = new GroupAllRespParam();
			User user = this.userDao.findById(group.getGroupCreatedBy());
			if ( group.getGroupState() == 0 ) {
				xmasInfo.setGroup_status("ready");
			} else if ( group.getGroupState() == 1 ) {
				xmasInfo.setGroup_status("started");
			} else if ( group.getGroupState() == 2 ) {
				xmasInfo.setGroup_status("finished");
			}
			
			Event event = this.eventDao.findByGroupId(group.getGroupId(), persist.getUserId());	
			if ( event == null ) {
				xmasInfo.setUser_status("requestable");
			} else if ( event.getUserStts() == 0 ) {
				xmasInfo.setUser_status("requested");
			} else if ( event.getUserStts() == 1 && group.getGroupCreatedBy() == persist.getUserId() ) {
				xmasInfo.setUser_status("leadership");
			} else if ( event.getUserStts() == 1 ) {
				xmasInfo.setUser_status("membership");
			}
			
			List<Event> eventList = this.eventDao.findByGroupId(group.getGroupId());
			int countPlayers = 0;
			for ( Event evnt : eventList ) {
				if ( evnt.getUserStts() == 1 ) {
					countPlayers = countPlayers + 1;
				}
			}
			
			countNo = countNo+1;
			xmasInfo.setNo(countNo);
			xmasInfo.setPlayers(countPlayers);
			xmasInfo.setGroup_name(group.getGroupName());
			xmasInfo.setCreated_datetime(formatTime(group.getGroupDttm()));
			xmasInfo.setCreated_by_id(group.getGroupCreatedBy());
			xmasInfo.setCreated_by(user.getFirstName()+ " " +user.getLastName());
			xmasInfo.setDescription(group.getDescription());
			xmasResponseList.add(xmasInfo);
		}
		
		Integer allPage = null;
		Integer page = null;
		try {
			Integer countGroup = this.groupDao.findCountAllGroup(request).size();
			double resultAllPage = (double) countGroup/request.getLimit();
			allPage = (int) Math.ceil(resultAllPage);
			allPage = allPage == 0 ? 1 : allPage;
			
			double resultPage = ((double) request.getOffset()/request.getLimit())+1;
			page = (int) Math.ceil(resultPage);
			
		}catch(Exception e) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_UNEXPECTED, "Error Division GroupController Method GET");
		}
		
		GenericRestResponse<List<GroupAllRespParam>> response = new GenericRestResponse<List<GroupAllRespParam>>();
		response.setData(xmasResponseList);
		response.setPage(page);
		response.setNumberOfPages(allPage);
		return this.gson.toJson(response);
	}
	
	public String groupNote(String desc, String groupName, String token) throws ServiceException {
		PersistentLogin persist = this.authorizeDao.findRoleAdmin(token);
		Group group = this.groupDao.findByGroupName(groupName);
		
		if (group.getGroupCreatedBy() == persist.getUserId()) {
			this.groupDao.updateGroupNote(group.getGroupId(), desc);
		} else {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_ACCESS_DENIED, ErrorConstant.ERR_DESC_ACCESS_DENIED);
		}
		
		return this.gson.toJson(new GenericRestResponse<String>());
	}
	
	/* ADMIN delete group ...group-state[0=ready, 1=started, 2=finished] */
	public String deleteById(GroupInfo groupInfo, String token) throws ServiceException {
		if ( groupInfo.getGroupId() == null ) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_UNEXPECTED, ErrorConstant.ERR_DESC_UNEXPECTED);
		}
		
		PersistentLogin persist = this.authorizeDao.findRoleAdmin(token);		
		Group group = groupDao.findById(groupInfo.getGroupId());
		if (group != null) {
			//if(user.getUserId() == group.getGroupCreatedBy() && group.getGroupState() == 0 || group.getGroupState() == 2){
			if("ADMIN".equals(persist.getRoleName()) && ( group.getGroupState() == Constant.GROUP_STATE_READY || group.getGroupState() == Constant.GROUP_STATE_FINISH )) {
				this.eventDao.deleteByGroupId(group.getGroupId());
				this.groupDao.deleteById(group.getGroupId());
			} else {
				throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_YOU_CANNOT_DELETE, ErrorConstant.ERR_DESC_YOU_CANNOT_DELETE);
			}
		}
		return this.gson.toJson(new GenericRestResponse<String>());
	}
	
	public String formatTime(Date date) {
		SimpleDateFormat formatTime = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
		return formatTime.format(date);
	}
}
