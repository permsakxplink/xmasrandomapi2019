package com.xplink.api.xmas.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xplink.api.exception.ServiceException;
import com.xplink.api.rest.controller.AbstractRestController;
import com.xplink.api.rest.controller.vo.RestHeaderRequest;
import com.xplink.api.xmas.dao.GroupDao;
import com.xplink.api.xmas.service.RandomService;

@RestController
@RequestMapping("/christmas-random-api/random")
public class RandomController extends AbstractRestController {
	
	@Autowired
	RandomService randomService;
	
	@Autowired
	GroupDao groupDao;
	
	@Override
	protected String get(RestHeaderRequest header, Map<String, String> dataMap) throws ServiceException {
		if("event-finish".equals(header.getBizService())){
			String groupName = dataMap.get("group_name");
			return randomService.randomFinish(groupName, header.getToken());
		} else if("event-start".equals(header.getBizService())) {
			return randomService.random(header, dataMap.get("group_name"));
		}
		return null;
	}

	@Override
	protected String post(RestHeaderRequest header, String request) throws ServiceException {
		return null;
	}

	@Override
	protected String put(RestHeaderRequest header, String request) throws ServiceException {
		return null;
	}

	@Override
	protected String delete(RestHeaderRequest header, String request) throws ServiceException {
		return null;
	}

}
