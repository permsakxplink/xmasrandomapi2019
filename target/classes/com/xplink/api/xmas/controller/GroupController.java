package com.xplink.api.xmas.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xplink.api.exception.ServiceException;
import com.xplink.api.rest.constant.ErrorConstant;
import com.xplink.api.rest.controller.AbstractRestController;
import com.xplink.api.rest.controller.vo.RestHeaderRequest;
import com.xplink.api.xmas.controller.vo.GroupAllReqs;
import com.xplink.api.xmas.controller.vo.GroupInfo;
import com.xplink.api.xmas.dao.GroupDao;
import com.xplink.api.xmas.dao.UserDao;
import com.xplink.api.xmas.service.EventService;
import com.xplink.api.xmas.service.GroupService;

@RestController
@RequestMapping("/christmas-random-api/groups")
public class GroupController extends AbstractRestController {

	@Autowired
	GroupService groupService;

	@Autowired
	EventService eventService;
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	GroupDao groupDao;

	@Override
	protected String get(RestHeaderRequest header, Map<String, String> dataMap) throws ServiceException {
		log_info("GroupController GET method.");
		if (header.getBizService().indexOf("all-group") > -1) {
			GroupAllReqs request = new GroupAllReqs();
			Integer limit = dataMap.get("data_per_page") == null ? 5 : Integer.valueOf(dataMap.get("data_per_page"));
			Integer offset = dataMap.get("start") == null ? 0 : Integer.valueOf(dataMap.get("start"));
			request.setLimit(limit);
			request.setOffset(offset);
			request.setSortBy(dataMap.get("sort_by"));
			request.setSortField(dataMap.get("sort_field"));
			request.setSearch(dataMap.get("search"));
			return this.groupService.findByLimit(request, header.getToken(), header.getBizService());	
		} else if ("request-group".equals(header.getBizService())) {
			String groupName = dataMap.get("group_name");
			String email = dataMap.get("invite_by_email") == null ? null : dataMap.get("invite_by_email");
			return this.groupService.requestGroup(email, groupName, header);
		}else if ("accept-request-group".equals(header.getBizService())) {
			String groupName = dataMap.get("group_name");
			Integer userId = dataMap.get("user_id") == null ? null : Integer.valueOf(dataMap.get("user_id"));
			String accept = dataMap.get("accept");
			if ( userId == null ) {
				return this.groupService.acceptGroup(accept, groupName, header.getToken());
			} else {
				return this.groupService.acceptGroup(accept, groupName, userId, header.getToken());
			}
		} else if ("join-group".equals(header.getBizService())) {
			String groupName = dataMap.get("group_name");
			Integer userId = dataMap.get("user_id") == null ? null : Integer.valueOf(dataMap.get("user_id"));
			log_debug("******************************");
			log_debug("User ID =" + userId);
			log_debug("Group Name =" + groupName);
			if ( userId == null ) {
				throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_UNEXPECTED, ErrorConstant.ERR_DESC_UNEXPECTED);
			} else {
				return this.groupService.joinGroup(groupName, userId, header.getToken());
			}
		} else {
			return null;
		}
	}

	@Override
	protected String post(RestHeaderRequest header, String request) throws ServiceException {
		log_info("GroupController POST Method.");			
		Map<String, String> map= new HashMap<String, String>();
		map = this.gson.fromJson(request, map.getClass());				
		String groupName = map.get("group_name");
				
		if( groupName == null || "".equals(groupName)) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_UNEXPECTED, ErrorConstant.ERR_DESC_UNEXPECTED);
		}
				
		return this.groupService.create(groupName, header.getToken());
	}

	@Override
	protected String put(RestHeaderRequest header, String request) throws ServiceException {
		log_info("GroupController POST Method.");			
		Map<String, String> map= new HashMap<String, String>();
		map = this.gson.fromJson(request, map.getClass());				
		String groupName = map.get("group_name");
		String desc = map.get("description");
		if("group-note".equals(header.getBizService())) {
			return this.groupService.groupNote(desc, groupName, header.getToken());
		}
		return null;
	}

	@Override
	protected String delete(RestHeaderRequest header, String request) throws ServiceException {
		return this.groupService.deleteById(this.gson.fromJson(request, GroupInfo.class), header.getToken());
	}
}
