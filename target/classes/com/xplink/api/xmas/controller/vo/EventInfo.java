package com.xplink.api.xmas.controller.vo;

import java.util.Date;

public class EventInfo {

	private Integer evtId;
	private String keyword;
	private String receive_keyword;
	private Date LastChngDttm;
	private Integer groupId;
	private String groupName;
	private Integer userId;
	private Integer userStts;
	
	public String getReceive_keyword() {
		return receive_keyword;
	}

	public void setReceive_keyword(String receive_keyword) {
		this.receive_keyword = receive_keyword;
	}

	public Date getLastChngDttm() {
		return LastChngDttm;
	}

	public void setLastChngDttm(Date lastChngDttm) {
		LastChngDttm = lastChngDttm;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Integer getUserStts() {
		return userStts;
	}

	public void setUserStts(Integer userStts) {
		this.userStts = userStts;
	}

	public Integer getEvtId() {
		return evtId;
	}

	public void setEvtId(Integer evtId) {
		this.evtId = evtId;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer evtGroupId) {
		this.groupId = evtGroupId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer evtUserId) {
		this.userId = evtUserId;
	}

}
