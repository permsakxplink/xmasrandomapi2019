package com.xplink.api.xmas.controller.vo;

public class EmailInfo {

	private String biz;
	private String usernameInvite;
	private String groupName;
	private String username;
	private String password;
	private String firstName;
	private String lastName;

	public String getBiz() {
		return biz;
	}

	public void setBiz(String biz) {
		this.biz = biz;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsernameInvite() {
		return usernameInvite;
	}

	public void setUsernameInvite(String usernameInvite) {
		this.usernameInvite = usernameInvite;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
}
