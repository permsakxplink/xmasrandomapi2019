package com.xplink.api.xmas.controller.vo;

import java.util.Date;

public class RandomInfo {
	
	private Integer randId;
	private Date randDttm;
	private Integer randState;
	private String player;
	private String player_keyword;
	private String provider;
	private String provider_keyword;
	
	public String getPlayer() {
		return player;
	}
	public void setPlayer(String player) {
		this.player = player;
	}
	public String getPlayer_keyword() {
		return player_keyword;
	}
	public void setPlayer_keyword(String player_keyword) {
		this.player_keyword = player_keyword;
	}
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public String getProvider_keyword() {
		return provider_keyword;
	}
	public void setProvider_keyword(String provider_keyword) {
		this.provider_keyword = provider_keyword;
	}
	public Integer getRandId() {
		return randId;
	}
	public void setRandId(Integer randId) {
		this.randId = randId;
	}
	public Date getRandDttm() {
		return randDttm;
	}
	public void setRandDttm(Date randDttm) {
		this.randDttm = randDttm;
	}
	public Integer getRandState() {
		return randState;
	}
	public void setRandState(Integer randState) {
		this.randState = randState;
	}
}
