package com.xplink.api.xmas.controller.vo;

import java.util.List;

public class Captcha {

	private Boolean success;
	private List<String> errorCodes;
	private String challenge_ts;
	private String hostname;

	public Boolean isSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public List<String> getErrorCodes() {
		return errorCodes;
	}

	public void setErrorCodes(List<String> errorCodes) {
		this.errorCodes = errorCodes;
	}

	public String getChallenge_ts() {
		return challenge_ts;
	}

	public void setChallenge_ts(String challenge_ts) {
		this.challenge_ts = challenge_ts;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	@Override
	public String toString() {
		return "Captcha [success=" + success + ", errorCodes=" + errorCodes + ", challenge_ts=" + challenge_ts
				+ ", hostname=" + hostname + "]";
	}	
}
