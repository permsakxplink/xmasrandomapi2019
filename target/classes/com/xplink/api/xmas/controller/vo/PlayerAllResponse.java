package com.xplink.api.xmas.controller.vo;

import java.util.List;

public class PlayerAllResponse {
	
	private String room_name;
	private String created_by;
	private Integer created_by_id;
	private String created_at;
	private String group_status;
	private String keyword_status;
	private String description;
	private List<PlayerPlyResponseParams> players;
	private List<PlayerReqsResponseParams> requests;

	public Integer getCreated_by_id() {
		return created_by_id;
	}

	public void setCreated_by_id(Integer created_by_id) {
		this.created_by_id = created_by_id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getKeyword_status() {
		return keyword_status;
	}

	public void setKeyword_status(String keyword_status) {
		this.keyword_status = keyword_status;
	}

	public String getGroup_status() {
		return group_status;
	}

	public void setGroup_status(String group_status) {
		this.group_status = group_status;
	}

	public String getRoom_name() {
		return room_name;
	}

	public void setRoom_name(String room_name) {
		this.room_name = room_name;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public List<PlayerPlyResponseParams> getPlayers() {
		return players;
	}

	public void setPlayers(List<PlayerPlyResponseParams> players) {
		this.players = players;
	}

	public List<PlayerReqsResponseParams> getRequests() {
		return requests;
	}

	public void setRequests(List<PlayerReqsResponseParams> requests) {
		this.requests = requests;
	}
}
