package com.xplink.api.xmas.controller;

import java.util.Map;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xplink.api.exception.ServiceException;
import com.xplink.api.rest.controller.AbstractRestController;
import com.xplink.api.rest.controller.vo.RestHeaderRequest;
import com.xplink.api.xmas.controller.vo.AuthorizeInfo;

@RestController
@RequestMapping("/christmas-random-api/authen")
public class AuthorizeController extends AbstractRestController {

	@Override
	protected String get(RestHeaderRequest header, Map<String, String> dataMap) throws ServiceException {
		return null;
	}

	@Override
	protected String post(RestHeaderRequest header, String request) throws ServiceException {
		AuthorizeInfo authorizeInfo = this.gson.fromJson(request, AuthorizeInfo.class);
		String username = authorizeInfo.getUsername();
		String password = authorizeInfo.getPassword();
		return this.authorizeService.authen(username, password);
	}

	@Override
	protected String put(RestHeaderRequest header, String request) throws ServiceException {
		return null;
	}

	@Override
	protected String delete(RestHeaderRequest header, String request) throws ServiceException {
		AuthorizeInfo authorizeInfo = this.gson.fromJson(request, AuthorizeInfo.class);
		return this.authorizeService.deleteByUsername(authorizeInfo.getUsername());
	}

}
