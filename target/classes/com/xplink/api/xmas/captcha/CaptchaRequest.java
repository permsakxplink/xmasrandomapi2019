package com.xplink.api.xmas.captcha;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;

import com.google.gson.Gson;
import com.xplink.api.log.AbstractLogManager;
import com.xplink.api.xmas.controller.vo.Captcha;
import com.xplink.api.xmas.controller.vo.CaptchaInfo;

public class CaptchaRequest extends AbstractLogManager{
	
	private Gson gson = new Gson();
	
	final static String SECRET = "6LdOtjoUAAAAAK_JsipJJaeu4hH5e1ZEGMXbQocf";

	public Captcha CapchaCalling(CaptchaInfo captchaInfo) throws UnsupportedOperationException, IOException, Exception {
		log_info("Request captcha by HttpClient");
		
		String url = "https://www.google.com/recaptcha/api/siteverify";
		HttpClient client = HttpClientBuilder.create().build();
		HttpPost httpPost = new HttpPost(url);
		log_info("url="+url);

		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("secret", SECRET));
		urlParameters.add(new BasicNameValuePair("response", captchaInfo.getResponse()));
		urlParameters.add(new BasicNameValuePair("remoteip", captchaInfo.getRemoteip()));

		httpPost.setEntity(new UrlEncodedFormEntity(urlParameters));

		HttpResponse response = client.execute(httpPost);
		return buildResponse(response.getEntity().getContent());
	}

	public Captcha buildResponse(InputStream reply) throws Exception {

		BufferedReader reader = new BufferedReader(new InputStreamReader(reply));
		StringBuilder builder = new StringBuilder();

			while (true) {
				String line = reader.readLine();
				log_info("Captcha response =" +line);
				if (line == null) {
					break;
				} else {
					builder.append(line);
				}
			}
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap = this.gson.fromJson(builder.toString(), Map.class);
			log_info("Map toString =" +dataMap.toString());
			
			Captcha captcha = new Captcha();
			captcha.setSuccess( dataMap.get("success") == null ? null : Boolean.valueOf((boolean) dataMap.get("success")) );
			captcha.setHostname( dataMap.get("hostname") == null ? null : dataMap.get("hostname").toString() );
			captcha.setChallenge_ts( dataMap.get("challenge_ts") == null ? null : dataMap.get("challenge_ts").toString() );
 			captcha.setErrorCodes( (List<String>) dataMap.get("error-codes") );
			log_info("Captcha Model response =" +captcha.toString());
			
			return captcha;
	}
}
