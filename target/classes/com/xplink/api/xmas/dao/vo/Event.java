package com.xplink.api.xmas.dao.vo;

import java.io.Serializable;
import java.util.Date;

public class Event implements Serializable {

	private static final long serialVersionUID = 8690173435583258151L;

	private Integer evtId;
	private String evtKeyword;
	private Date evtLastChngDttm;
	private String evtReceiveKeyword;
	private Integer groupId;
	private Integer userId;
	private Integer userStts;

	public Integer getUserStts() {
		return userStts;
	}

	public void setUserStts(Integer userStts) {
		this.userStts = userStts;
	}

	public Integer getEvtId() {
		return evtId;
	}

	public void setEvtId(Integer evtId) {
		this.evtId = evtId;
	}

	public String getEvtKeyword() {
		return evtKeyword;
	}

	public void setEvtKeyword(String evtKeyword) {
		this.evtKeyword = evtKeyword;
	}

	public Date getEvtLastChngDttm() {
		return evtLastChngDttm;
	}

	public void setEvtLastChngDttm(Date evtLastChngDttm) {
		this.evtLastChngDttm = evtLastChngDttm;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getEvtReceiveKeyword() {
		return evtReceiveKeyword;
	}

	public void setEvtReceiveKeyword(String evtReceiveKeyword) {
		this.evtReceiveKeyword = evtReceiveKeyword;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
