package com.xplink.api.xmas.dao.vo;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {

	@Override
	public String toString() {
		return "User [usr_id=" + userId + ", username=" + username + ", password=" + password + ", first_name="
				+ firstName + ", last_name=" + lastName + ", last_chng_dttm=" + lastChngDttm + ", role_name=" + roleName
				+ ", keyword_id=" + keywordId + "]";
	}

	private static final long serialVersionUID = -3183701655025062977L;

	private Integer userId;
	private String username;
	private String password;
	private String firstName;
	private String lastName;
	private Date lastChngDttm;
	private String roleName;
	private Integer keywordId;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getLastChngDttm() {
		return lastChngDttm;
	}

	public void setLastChngDttm(Date lastChngDttm) {
		this.lastChngDttm = lastChngDttm;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Integer getKeywordId() {
		return keywordId;
	}

	public void setKeywordId(Integer keywordId) {
		this.keywordId = keywordId;
	}

}
