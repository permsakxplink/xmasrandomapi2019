package com.xplink.api.xmas.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.List;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xplink.api.config.sql.SQLPropertyFileConfig;
import com.xplink.api.dao.AbstractDao;
import com.xplink.api.rest.constant.SQLConstant;
import com.xplink.api.xmas.controller.vo.PlayersInfo;
import com.xplink.api.xmas.dao.rowmapper.PlayersRowMapper;
import com.xplink.api.xmas.dao.rowmapper.UserRowMapper;
import com.xplink.api.xmas.dao.vo.User;

@Repository
@Transactional
public class UserDao extends AbstractDao {

	public int create(User user) {
		log_info("Create User=" + user.getUsername());
		String sql = this.sqlConfig.getSql(SQLConstant.INSERT_USER_KEY);
		log_info(SQLConstant.INSERT_USER_KEY + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));
		
		KeyHolder keyHolder = new GeneratedKeyHolder();
		int rowUpdate = getJdbcTemplate().update(new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int i = 1;
				ps.setString(i++, user.getUsername());
				ps.setString(i++, user.getPassword());
				ps.setString(i++, user.getFirstName());
				ps.setString(i++, user.getLastName());
				ps.setTimestamp(i++, new Timestamp(user.getLastChngDttm().getTime()));
				return ps;
			}
		}, keyHolder);
		
		Integer newId;
	    if (keyHolder.getKeys().size() > 1) {
	        newId = (Integer)keyHolder.getKeys().get("usr_id");
	    } else {
	        newId= keyHolder.getKey().intValue();
	    }
		int id = newId.intValue();
		log_info("Inserted " + rowUpdate + " row(s), id=" + id);
		return id;
	}
	
	public void update(User user) {
		log_info("Update User=" + user.getUsername());
		String sql = this.sqlConfig.getSql(SQLConstant.UPDATE_USER_KEY);
		log_info(SQLConstant.UPDATE_USER_KEY + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));
		
		Object[] parms = {
				user.getFirstName(),
				user.getLastName(),
				user.getPassword(),
				user.getLastChngDttm(),
				user.getUserId()
				};
		
		int[] types = {
				Types.VARCHAR,
				Types.VARCHAR,
				Types.VARCHAR,
				Types.TIMESTAMP,
				Types.INTEGER
				};
		
		int rowUpdate = getJdbcTemplate().update(sql, parms, types);
		log_info("Updated " + rowUpdate + " row(s)");
	}
	
	public User findByUsername(String username) {
		log_info("Username=" + username);
		try {
			String sql = this.sqlConfig.getSql(SQLConstant.SELECT_USER_BY_NAME_KEY);
			log_info(SQLConstant.SELECT_USER_BY_NAME_KEY + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));

			Object[] parms = {username};
			int[] types = {Types.VARCHAR};
			
			return getJdbcTemplate().queryForObject(sql, parms, types, new UserRowMapper());
		} catch (EmptyResultDataAccessException e) {
			log_error("User is not found");
			return null;
		} catch (Exception e) {
			log_error("User is not found", e);
			return null;
		}
	}
	
	public User findById(Integer userId) {
		log_info("UserId=" + userId);
		try {
			String sql = this.sqlConfig.getSql(SQLConstant.SELECT_USER_BY_ID_KEY);
			log_info(SQLConstant.SELECT_USER_BY_ID_KEY + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));

			Object[] parms = {userId};
			int[] types = {Types.INTEGER};
			
			return getJdbcTemplate().queryForObject(sql, parms, types, new UserRowMapper());
		} catch (EmptyResultDataAccessException e) {
			log_error("User is not found");
			return null;
		} catch (Exception e) {
			log_error("User is not found", e);
			return null;
		}
	}
	
	public List<User> findAllUsers() {
		log_info("Find all users");
		try {
			String sql = this.sqlConfig.getSql(SQLConstant.SELECT_ALL_USER_KEY);
			log_info(SQLConstant.SELECT_ALL_USER_KEY + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));
			
			List<User> list = getJdbcTemplate().query(sql, new UserRowMapper());
			int size = (list == null) ? 0 : list.size();
			log_debug("User list size=" + size);
			
			return list;
		} catch (Exception e) {
			log_error("All User is not found", e);
			return null;
		}
	}
	
	public void deleteById(Integer userId) {
		log_info("Delete UserId=" + userId);
		String sql = this.sqlConfig.getSql(SQLConstant.DELETE_USER_BY_ID_KEY);
		log_info(SQLConstant.DELETE_USER_BY_ID_KEY + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));
		
		Object[] parms = {userId};
		int[] types = {Types.INTEGER};
		
		int rowUpdate = getJdbcTemplate().update(sql, parms, types);
		log_info("Deleted " + rowUpdate + " row(s)");
	}
	
	public User findByFullName(String firstName, String lastName) {
		log_info("Full Name = " + firstName + " " + lastName);
		try {
			String sql = this.sqlConfig.getSql(SQLConstant.SELECT_USER_BY_FULL_NAME);
			log_info(SQLConstant.SELECT_USER_BY_FULL_NAME + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));

			Object[] parms = {firstName, lastName};
			int[] types = {Types.VARCHAR, Types.VARCHAR};
			
			return getJdbcTemplate().queryForObject(sql, parms, types, new UserRowMapper());
		} catch (EmptyResultDataAccessException e) {
			log_error("Full name is not found");
			return null;
		} catch (Exception e) {
			log_error("Full name is not found", e);
			return null;
		}
	}
}
