package com.xplink.api.xmas.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.xplink.api.xmas.dao.vo.Group;

public class GroupRowMapper implements RowMapper<Group>{

	@Override
	public Group mapRow(ResultSet rs, int rowNum) throws SQLException {
		Group group = new Group();
		group.setGroupId(rs.getInt("grp_id"));
		group.setGroupName(rs.getString("grp_name"));
		group.setGroupDttm(rs.getTimestamp("grp_dttm"));
		group.setGroupState(rs.getInt("grp_state"));
		group.setGroupCreatedBy(rs.getInt("grp_createdby"));
		group.setUsername(rs.getString("username"));
		group.setPlayers(rs.getInt("players"));
		group.setDescription(rs.getString("grp_note"));
		return group;
	}
}
