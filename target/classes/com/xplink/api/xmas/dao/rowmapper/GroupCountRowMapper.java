package com.xplink.api.xmas.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.xplink.api.xmas.dao.vo.Group;

public class GroupCountRowMapper implements RowMapper<Group>{

	@Override	
	public Group mapRow(ResultSet rs, int rowNum) throws SQLException {
		Group group = new Group();
		group.setGroupName(rs.getString("g.grp_name"));
		return group;
	}
}
