package com.xplink.api.xmas.dao;

import java.sql.Types;
import java.util.Date;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

import com.xplink.api.config.sql.SQLPropertyFileConfig;
import com.xplink.api.dao.AbstractDao;
import com.xplink.api.rest.constant.SQLConstant;
import com.xplink.api.xmas.dao.rowmapper.PersistentLoginRowMapper;
import com.xplink.api.xmas.dao.vo.PersistentLogin;

@Repository
public class AuthorizeDao extends AbstractDao {

	public void createNewToken(PersistentLogin token) {
		log_info("Creating Token for user " + token.getUsername());
		String sql = this.sqlConfig.getSql(SQLConstant.INSERT_PERSIT_TOKEN_KEY);
		log_info(SQLConstant.INSERT_PERSIT_TOKEN_KEY + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));
		
		Object[] parms = {
			token.getUsername(),
			token.getSeries(),
			token.getToken(),
			token.getLastUsed()
		};
		
		int[] types = {
			Types.VARCHAR,
			Types.VARCHAR,
			Types.VARCHAR,
			Types.TIMESTAMP
		};
		
		int rowUpdate = getJdbcTemplate().update(sql, parms, types);
		log_info("Inserted " + rowUpdate + " row(s)");
	}

	public PersistentLogin getTokenForSeries(String seriesId) {
		log_info("Fetch Token if any for seriesId " + seriesId);
		try {
			String sql = this.sqlConfig.getSql(SQLConstant.SELECT_PERSIT_TOKEN_KEY);
			log_info(SQLConstant.SELECT_PERSIT_TOKEN_KEY + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));

			Object[] parms = { seriesId };
			int[] types = { Types.VARCHAR };
			
			PersistentLogin persistentLogin = getJdbcTemplate().queryForObject(sql, parms, types, new PersistentLoginRowMapper());
			log_debug(persistentLogin.toString());
			return persistentLogin;
		} catch (EmptyResultDataAccessException e) {
			log_error("Token not found");
			return null;
		} catch (Exception e) {
			log_error("Token not found", e);
			return null;
		}
	}
	
	public PersistentLogin getTokenByUsername(String username) {
		log_info("Fetch Token if any for username " + username);
		try {
			String sql = this.sqlConfig.getSql(SQLConstant.SELECT_PERSIT_TOKEN_BY_USERNAME_KEY);
			log_info(SQLConstant.SELECT_PERSIT_TOKEN_BY_USERNAME_KEY + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));

			Object[] parms = { username };
			int[] types = { Types.VARCHAR };
			
			PersistentLogin persistentLogin = getJdbcTemplate().queryForObject(sql, parms, types, new PersistentLoginRowMapper());
			log_debug(persistentLogin.toString());
			return persistentLogin;
		} catch (EmptyResultDataAccessException e) {
			log_error("Token not found");
			return null;
		} catch (Exception e) {
			log_error("Token not found", e);
			return null;
		}
	}
	
	public PersistentLogin findRoleAdmin(String token) {
		log_info("Find Role Admin by Token =" + token);
		try {
		String sql = this.sqlConfig.getSql(SQLConstant.SELECT_PERSIT_ROLE_BY_TOKEN);
		log_info(SQLConstant.SELECT_PERSIT_ROLE_BY_TOKEN + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));
		
		Object[] parms = {token};
			int[] types = {Types.VARCHAR};
		
		return getJdbcTemplate().queryForObject(sql, parms, types, new PersistentLoginRowMapper());
		} catch (Exception e) {
			log_error("Find Role Admin by Token is not found", e);
			return null;
		}
	}

	public void removeUserTokens(String username) {
		log_info("Removing Token if any for user " + username);
		String sql = this.sqlConfig.getSql(SQLConstant.DELETE_PERSIT_TOKEN_KEY);
		log_info(SQLConstant.DELETE_PERSIT_TOKEN_KEY + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));

		Object[] parms = { username };
		int[] types = { Types.VARCHAR };
		
		int rowUpdate = getJdbcTemplate().update(sql, parms, types);
		log_info("Deleted " + rowUpdate + " row(s)");
	}

    public void updateToken(String seriesId, String tokenValue, Date lastUsed) {
        log_info("Updating Token for seriesId=" + seriesId);
        String sql = this.sqlConfig.getSql(SQLConstant.UPDATE_PERSIT_TOKEN_KEY);
		log_info(SQLConstant.UPDATE_PERSIT_TOKEN_KEY + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));

		Object[] parms = { 
			tokenValue,
			lastUsed,
			seriesId 
		};
		
		int[] types = { 
			Types.VARCHAR,
			Types.TIMESTAMP,
			Types.VARCHAR
		};
		
		int rowUpdate = getJdbcTemplate().update(sql, parms, types);
		log_info("Updated " + rowUpdate + " row(s)");
    }
}
