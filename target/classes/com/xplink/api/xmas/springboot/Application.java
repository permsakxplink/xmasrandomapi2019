package com.xplink.api.xmas.springboot;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.net.ssl.SSLContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configurator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.ResourceUtils;
import org.springframework.web.client.RestTemplate;

/**
 * <p> Christmas Random </p>
 * @version 1.0
 * @author Phongsathorn Angyarn <phongsathorn@xp-link.com>
 * @since December, 2016
 *
 */

@Configuration
@EnableAutoConfiguration
@ComponentScan (basePackages = { "com.xplink.api.*" })
@SpringBootApplication
public class Application {
	
	protected Logger logger = LogManager.getLogger(this.getClass());
	
	@Autowired
	private Environment env;
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
    }
	
//	@Override
    public void run(String... args) throws Exception {
        final String password = "xplink";
        SSLContext sslContext = SSLContextBuilder
                .create()
                .loadTrustMaterial(ResourceUtils.getFile("classpath:keystore/baeldung.p12"), password.toCharArray())
                .build();
        CloseableHttpClient client = HttpClients.custom()
                .setSSLContext(sslContext)
                .build();
        
        HttpComponentsClientHttpRequestFactory requestFactory
                = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(client);
        
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        
        String url = "https://localhost:8443/status/check";
        
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, HttpEntity.EMPTY, String.class);
        
        System.out.println("Result = " + response.getBody());
    }	
	
	@Bean
	public ServletListenerRegistrationBean<ServletContextListener> servletContextListener() {

	    ServletContextListener listener = new ServletContextListener() {
	        @Override
	        public void contextInitialized(ServletContextEvent sce) {
	        	sys_log_info("Initialized: " + env.getProperty("app.name"));
	        	sys_log_info("server.port: " + env.getProperty("server.port"));
	        	sys_log_info("spring.profiles.active: " + env.getProperty("spring.profiles.active"));
	        	sys_log_info("spring.config.location: " + env.getProperty("spring.config.location"));
	        }

	        @Override
	        public void contextDestroyed(ServletContextEvent sce) {
	        	sys_log_info("Destroyed: " + env.getProperty("app.name"));
	        	LoggerContext context = (LoggerContext) LogManager.getContext();
	        	Configurator.shutdown(context);
	        }
	    };

	    return new ServletListenerRegistrationBean<ServletContextListener>(listener);
	}
	
//	private void loadLog4jConfig() {
//		sys_log_info("Log4jFilePath=" + log4jFilePath);
//		try (FileInputStream fin = new FileInputStream(this.log4jFilePath)){
//			ConfigurationSource source = new ConfigurationSource(fin);
//			Configurator.initialize(null, source);
//		} catch (Throwable e) {
//			sys_log_error("Cannot initialized log4j, error=" + e.getMessage());
//			e.printStackTrace();
//		}
//	}

	protected void sys_log_error(String msg) {
		System.out.println(getCurrentDate() + "  INFO 00000 --- [         XPLINK]                                          : " + msg);
	}
	
	protected void sys_log_info(String msg) {
		System.out.println(getCurrentDate() + "  INFO 00000 --- [         XPLINK]                                          : " + msg);
	}
	
	protected String getCurrentDate() {
		SimpleDateFormat formater = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss.SSS");
		return formater.format(Calendar.getInstance().getTime());
	}

}