-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 19, 2017 at 07:36 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `xmasgroup_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `xmas_event`
--

CREATE TABLE `xmas_event` (
  `evt_id` bigint(20) NOT NULL,
  `evt_kywd` varchar(255) DEFAULT NULL,
  `evt_last_chng_dttm` datetime DEFAULT NULL,
  `evt_receive_keyword` varchar(255) DEFAULT NULL,
  `xmas_group_grp_id` bigint(20) NOT NULL,
  `xmas_user_usr_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xmas_event`
--

INSERT INTO `xmas_event` (`evt_id`, `evt_kywd`, `evt_last_chng_dttm`, `evt_receive_keyword`, `xmas_group_grp_id`, `xmas_user_usr_id`) VALUES
(1, 'xmas-random-test', '2017-02-17 17:43:15', 'test-keyword-4', 6, 2),
(3, 'test-keyword', '2017-02-17 17:46:40', 'test-keyword-3', 6, 3),
(4, 'test-keyword-2', '2017-02-17 17:46:40', 'test-keyword', 6, 4),
(5, 'test-keyword-3', '2017-02-17 17:46:40', 'xmas-random-test', 6, 5),
(6, 'test-keyword-4', '2017-02-17 17:46:40', 'test-keyword-2', 6, 6);

-- --------------------------------------------------------

--
-- Table structure for table `xmas_group`
--

CREATE TABLE `xmas_group` (
  `grp_id` bigint(20) NOT NULL,
  `grp_name` varchar(255) NOT NULL,
  `grp_dttm` datetime NOT NULL,
  `grp_state` int(1) NOT NULL,
  `grp_createdby` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xmas_group`
--

INSERT INTO `xmas_group` (`grp_id`, `grp_name`, `grp_dttm`, `grp_state`, `grp_createdby`) VALUES
(6, 'test-group-create', '2017-02-15 17:08:45', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `xmas_persistent_login`
--

CREATE TABLE `xmas_persistent_login` (
  `series` varchar(255) NOT NULL,
  `last_used` datetime DEFAULT NULL,
  `token` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `xmas_persistent_login`
--

INSERT INTO `xmas_persistent_login` (`series`, `last_used`, `token`, `username`) VALUES
('5cec81f8-64e9-4d9a-95e5-3fbe624ceccc', '2017-02-15 23:30:21', '$2a$10$RMCgNKnjOJ3a2l3LwcASvedTejKCbA2.K.2FPN4y.jm0XjPlB1LxW', 'mrbankshart@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `xmas_user`
--

CREATE TABLE `xmas_user` (
  `usr_id` bigint(20) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(100) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `last_chng_dttm` datetime DEFAULT NULL,
  `role_name` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `xmas_user`
--

INSERT INTO `xmas_user` (`usr_id`, `username`, `password`, `first_name`, `last_name`, `last_chng_dttm`, `role_name`) VALUES
(1, 'admin', 'cGFzc3dvcmQ=', 'Admin', 'XPLink', '2017-01-10 10:51:10', 'ADMIN'),
(2, 'mrbankshart@gmail.com', 'cGFzc3dvcmQ=', 'aaa', 'aaa', '2017-01-10 10:51:10', 'USER'),
(3, 'kyo_chero@hotmail.com', 'cGFzc3dvcmQ=', 'bbb', 'bbb', '2017-01-10 10:51:10', 'USER'),
(4, 'ccc@ccc.com', 'cGFzc3dvcmQ=', 'ccc', 'ccc', '2017-01-10 10:51:10', 'USER'),
(5, 'ddd@ddd.com', 'cGFzc3dvcmQ=', 'ddd', 'ddd', '2017-01-10 10:51:10', 'USER'),
(6, 'eee@eee.com', 'cGFzc3dvcmQ=', 'eee', 'eee', '2017-01-10 10:51:10', 'USER');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `xmas_event`
--
ALTER TABLE `xmas_event`
  ADD PRIMARY KEY (`evt_id`),
  ADD KEY `fk_xmas_event_xmas_group_idx` (`xmas_group_grp_id`),
  ADD KEY `fk_xmas_event_xmas_user1_idx` (`xmas_user_usr_id`);

--
-- Indexes for table `xmas_group`
--
ALTER TABLE `xmas_group`
  ADD PRIMARY KEY (`grp_id`);

--
-- Indexes for table `xmas_persistent_login`
--
ALTER TABLE `xmas_persistent_login`
  ADD PRIMARY KEY (`series`),
  ADD UNIQUE KEY `UK_token` (`token`),
  ADD UNIQUE KEY `UK_username` (`username`);

--
-- Indexes for table `xmas_user`
--
ALTER TABLE `xmas_user`
  ADD PRIMARY KEY (`usr_id`),
  ADD UNIQUE KEY `UK_user_id` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `xmas_event`
--
ALTER TABLE `xmas_event`
  MODIFY `evt_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `xmas_group`
--
ALTER TABLE `xmas_group`
  MODIFY `grp_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `xmas_user`
--
ALTER TABLE `xmas_user`
  MODIFY `usr_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `xmas_event`
--
ALTER TABLE `xmas_event`
  ADD CONSTRAINT `fk_xmas_event_xmas_group` FOREIGN KEY (`xmas_group_grp_id`) REFERENCES `xmas_group` (`grp_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_xmas_event_xmas_user1` FOREIGN KEY (`xmas_user_usr_id`) REFERENCES `xmas_user` (`usr_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
