package com.xplink.api.config;

import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Configuration
public class AppConfiguration {

	@Autowired
	private Environment env;
	
	@Bean
	public Gson gson() {
		Gson gson = new GsonBuilder()
				.disableHtmlEscaping()
				.create();
		return gson;
	}
	
	@Bean(destroyMethod="close")
	public DataSource dataSource() {
		BasicDataSource ds = new BasicDataSource();
		ds.setUrl(this.env.getProperty("datasource.url"));
		ds.setDriverClassName(this.env.getProperty("datasource.driverClassName"));
		ds.setUsername(this.env.getProperty("datasource.username"));
		ds.setPassword(this.env.getProperty("datasource.password"));
		ds.setInitialSize(Integer.parseInt(this.env.getProperty("datasource.pool.init")));
		ds.setMaxActive(Integer.parseInt(this.env.getProperty("datasource.pool.max")));
		ds.setTestOnBorrow(true);
		ds.setTestWhileIdle(true);
		ds.setTimeBetweenEvictionRunsMillis(60000);
		ds.setValidationQuery("SELECT 1");
		return ds;
	}
	
	@Bean
	public JavaMailSender mailSender() {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setHost(this.env.getProperty("mail.host"));
		mailSender.setPort(Integer.parseInt(this.env.getProperty("mail.port")));
		mailSender.setUsername(this.env.getProperty("mail.username"));
		mailSender.setPassword(this.env.getProperty("mail.password"));
		
		Properties prop = new Properties();
		prop.setProperty("mail.smtp.auth", "true");
		prop.setProperty("mail.smtp.starttls.enable", "true");
		mailSender.setJavaMailProperties(prop);
		return mailSender;
	}
	
	@Bean
	public ThreadPoolExecutor threadPoolExecutor() {
		int corePoolSize = 20;
		int maxPoolSize = 20;
		int keepAliveTimeMills = 10000;
		int queueSize = 50;
		ArrayBlockingQueue<Runnable> blockingQueue = new ArrayBlockingQueue<Runnable>(queueSize, true);
		ThreadPoolExecutor executor = new ThreadPoolExecutor(corePoolSize, maxPoolSize, keepAliveTimeMills, TimeUnit.MILLISECONDS, blockingQueue);
		executor.allowCoreThreadTimeOut(true);
		executor.setKeepAliveTime(keepAliveTimeMills, TimeUnit.MILLISECONDS);
        return executor;
	}
	
}
