package com.xplink.api.xmas.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xplink.api.exception.ServiceException;
import com.xplink.api.rest.controller.AbstractRestController;
import com.xplink.api.rest.controller.vo.RestHeaderRequest;
import com.xplink.api.xmas.controller.vo.UserInfo;
import com.xplink.api.xmas.service.UserService;


@RestController
@RequestMapping("/christmas-random-api/user")
public class UserController extends AbstractRestController {

	@Autowired
	private UserService userService;
	
	@Override
	protected String get(RestHeaderRequest header, Map<String, String> dataMap) throws ServiceException {
		log_info("UserController GET method.");
		UserInfo userInfo = new UserInfo();
		if ( "forget-password".equals(header.getBizService()) ) {
			userInfo.setUsername(dataMap.get("username"));
			return this.userService.forgetPassword(header, userInfo);
		}
		
		return null;
	}

	@Override
	protected String post(RestHeaderRequest header, String request) throws ServiceException {
		UserInfo userInfo = this.gson.fromJson(request, UserInfo.class);
		Map<String, String> map= new HashMap<String, String>();
		map = this.gson.fromJson(request, map.getClass());
		String recaptcha = map.get("g-recaptcha-response");
		return this.userService.acceptCreateByEmail(recaptcha, userInfo, header);
	}

	@Override
	protected String put(RestHeaderRequest header, String request) throws ServiceException {
		Map<String, String> map= new HashMap<String, String>();
		map = this.gson.fromJson(request, map.getClass());
		UserInfo userInfo = new UserInfo();
		userInfo.setFirstName(map.get("first_name"));
		userInfo.setLastName(map.get("last_name"));
		userInfo.setPassword(map.get("old_password"));
		userInfo.setNewPassword(map.get("new_password"));
		
		return this.userService.update(userInfo, header.getToken());
	}

	@Override
	protected String delete(RestHeaderRequest header, String request) throws ServiceException {
		UserInfo userInfo = this.gson.fromJson(request, UserInfo.class);
		return this.userService.delete(userInfo.getUserId(), header.getToken());
	}

}
