package com.xplink.api.xmas.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xplink.api.exception.ServiceException;
import com.xplink.api.rest.controller.AbstractRestController;
import com.xplink.api.rest.controller.vo.RestHeaderRequest;
import com.xplink.api.xmas.controller.vo.DelEventInfo;
import com.xplink.api.xmas.controller.vo.EventInfo;
import com.xplink.api.xmas.service.EventService;

@RestController
@RequestMapping("/christmas-random-api/event")
public class EventController extends AbstractRestController {

	@Autowired
	private EventService eventService;
	
	@Override
	protected String get(RestHeaderRequest header, Map<String, String> dataMap) throws ServiceException{			
		if ("event-edit-kywd".equals(header.getBizService())) {
			EventInfo eventInfo = new EventInfo();
			eventInfo.setKeyword(dataMap.get("keyword"));
			eventInfo.setGroupName(dataMap.get("groupName"));
			eventInfo.setReceive_keyword(dataMap.get("receive_keyword"));
			log_debug("Keyword is : " + eventInfo.getKeyword() + "  Group name is : " + eventInfo.getGroupName());
			return this.eventService.update(eventInfo, header.getToken());
			
		} else if ("event-get-keyword".equals(header.getBizService())) {
			String groupName = dataMap.get("group_name");
			return this.eventService.showKeyword(groupName, header.getToken());
			
		} else if ("event-exit-group".equals(header.getBizService())) {
			Integer userId = dataMap.get("user_id") == null ? null : Integer.valueOf(dataMap.get("user_id"));
			String groupName = dataMap.get("group_name");
			return this.eventService.exitGroup(userId, groupName, header.getToken());
			
		} else if ("event-players-list".equals(header.getBizService())) {
			String groupName = dataMap.get("group_name");
			return this.eventService.findPlayersByGroupId(groupName, header.getToken());
		}
			
		return null;
	}

	@Override
	protected String post(RestHeaderRequest header, String request) throws ServiceException {
		return null;
	}

	@Override
	protected String put(RestHeaderRequest header, String request) throws ServiceException {
		return null;
	}

	@Override
	protected String delete(RestHeaderRequest header, String request) throws ServiceException {
		return this.eventService.deleteByUserId(this.gson.fromJson(request, DelEventInfo.class));
	}

}
