package com.xplink.api.xmas.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.Base64Utils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.xplink.api.exception.ServiceException;
import com.xplink.api.log.AbstractLogManager;
import com.xplink.api.log.vo.TransactionLogVo;
import com.xplink.api.rest.constant.Constant;
import com.xplink.api.rest.constant.ErrorConstant;
import com.xplink.api.rest.controller.vo.GenericRestResponse;
import com.xplink.api.rest.controller.vo.RestHeaderRequest;
import com.xplink.api.xmas.controller.vo.EmailInfo;
import com.xplink.api.xmas.controller.vo.UserInfo;
import com.xplink.api.xmas.dao.UserDao;
import com.xplink.api.xmas.dao.vo.User;
import com.xplink.api.xmas.service.EmailService;
import com.xplink.api.xmas.service.UserService;

@RestController
@RequestMapping("/email")
@CrossOrigin(origins = "*", maxAge = 3600)
public class EmailController extends AbstractLogManager {
	
	@Value("${app.name}")
	private String appName;
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	protected Gson gson;
	
	private Map<String, String> buildGetRequest(RestHeaderRequest header, HttpServletRequest httpRequest) {
		Map<String, String> dataMap = new HashMap<String, String>();
		Map<String, String[]> paramMap = httpRequest.getParameterMap();
		Set<Entry<String, String[]>> set = paramMap.entrySet();
		for (Entry<String, String[]> entry : set) {
			String key = entry.getKey();
			String[] values = entry.getValue();
			dataMap.put(key, values[0]);
		}
		
		return dataMap;
	}
	
	private String getUrlParamString(HttpServletRequest httpRequest) {
		StringBuilder bodyBuilder = new StringBuilder();
		Enumeration<String> params = httpRequest.getParameterNames(); 
		while(params.hasMoreElements()){
		String paramName = params.nextElement();
		bodyBuilder.append(paramName).append("=").append(httpRequest.getParameter(paramName)).append(",");
		}
		
		return bodyBuilder.length() == 0 ? bodyBuilder.toString() : (bodyBuilder.substring(0, (bodyBuilder.length() - 1)));
	}
	
	@RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public void get(HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws ServiceException, IOException {
		long startTime = System.currentTimeMillis();
		TransactionLogVo logVo = new TransactionLogVo();
		buildTransactionLogVo(logVo, httpRequest);
		initLogParam(httpRequest);
		String httpMethod = httpRequest.getMethod();
		String response = null;
		String errorMessage = null;
		log_info("Start process with method " + httpMethod);
		RestHeaderRequest header = null;
		String bizName = null;
		try {
			
			header = buildHeaderRequest(httpRequest);
			log_info("Receive=" + getEncryptData(header, getUrlParamString(httpRequest)));
			
			Map<String, String> dataMap = buildGetRequest(header, httpRequest);
			
			String param = new String(Base64Utils.decode(dataMap.get("data").getBytes()));
			log_debug("data="+ param);
			
			String[] paramArray = param.split(",");
			dataMap = new HashMap<String, String>();
		    for (int i = 0; i < paramArray.length; i++) {
		    	String[] data = paramArray[i].split("=");
		    	String key = data[0];
		    	String value = data[1];
		    	dataMap.put(key, value);
		    }
		
		    if ( dataMap.get("biz").equals("invite") ) {
		    	bizName = "invite";
		    	EmailInfo emailInfo = new EmailInfo();
		    	emailInfo.setGroupName(dataMap.get("group_name"));
			    emailInfo.setUsernameInvite(dataMap.get("user_invite"));
			    emailInfo.setUsername(dataMap.get("username"));
		    	response = this.emailService.responseInviteWithEmail(header, emailInfo);
		    } else if ( dataMap.get("biz").equals("create") ) {
		    	UserInfo userInfo = new UserInfo();
		    	userInfo.setUsername(dataMap.get("username"));
		    	userInfo.setPassword(dataMap.get("password"));
		    	userInfo.setFirstName(dataMap.get("first_name"));
		    	userInfo.setLastName(dataMap.get("last_name"));
		    	
		    	User user = this.userDao.findByUsername(userInfo.getUsername());
		    	if ( user != null ) {
					throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_REGISTER_ALREADY, ErrorConstant.ERR_DESC_REGISTER_ALREADY);
				}
		    	
		    	response = this.userService.create(userInfo);
		    }   
		
		} catch (ServiceException e) {
			errorMessage = e.getMessage();
			log_error("SERVICE_NAME:" + e.getServiceName() + "|ERRCODE:" + e.getErrorCode() + "|ERRMESSAGE:" + e.getMessage() + "|workingTime = " + (System.currentTimeMillis() - startTime));
			buildErrorResponse(logVo, e.getErrorCode(), errorMessage);
				
		} catch (Throwable e) {
			errorMessage = e.getMessage();
			String errorDesc = ErrorConstant.ERR_DESC_UNEXPECTED + ", message=" + errorMessage;
			log_error("SERVICE_NAME:" + this.getClass().getSimpleName() + "|ERRCODE:" + ErrorConstant.ERR_CODE_UNEXPECTED + "|ERRMESSAGE:" + errorDesc + "|workingTime = " + (System.currentTimeMillis() - startTime), e);
			buildErrorResponse(logVo, ErrorConstant.ERR_CODE_UNEXPECTED, errorDesc);
			
		} finally {
			
			httpResponse.setContentType("text/html;charset=UTF-8");
			PrintWriter out = httpResponse.getWriter();
			out.println("response= "+response.toString());
			if (errorMessage != null) {
				log_info(errorMessage);
				out.println("<script type=\"text/javascript\">");
				out.println("alert('" +errorMessage+ "');");
				out.println("location='" +header.getRequestURL()+ ":80/#!/signin';");
				out.println("</script>");
			}
			
			if ( response != null ) {
				log_info("Redirect");
				out.println("<script type=\"text/javascript\">");
				out.println("alert('Thank you. Hope you enjoy in Christmas Random 2019.');");
				String fromInviteFlag = "invite".equals(bizName) ? "?frominvite=1" : "";
//				out.println("location='" +header.getRequestURL()+ ":3000/#" + fromInviteFlag + "';");
				out.println("location='" +"https://christmas-2ba45.firebaseapp.com/#/" + fromInviteFlag + "';"); // change url for Web ui
				out.println("</script>");
			}
			
			log_info("Response=" + response);
			log_info("End process with method " + httpMethod + ", elapsed time = " + (System.currentTimeMillis() - startTime));
			clearLogParam();
		}
    }
	
	private void buildErrorResponse(TransactionLogVo logVo, String errorCode, String errorDesc) throws IOException {
		GenericRestResponse<String> response = new GenericRestResponse<String>();
		response.setStatus(ErrorConstant.STTS_ERROR);
		response.setErrorCode(errorCode);
		response.setErrorDesc(errorDesc);
		
		logVo.setStatus(ErrorConstant.STTS_ERROR);
		logVo.setErrorCode(errorCode);
	}
	private void buildTransactionLogVo(TransactionLogVo logVo, HttpServletRequest httpRequest) {
		
		String uuid = UUID.randomUUID().toString();
		String bizService = null;
		String method = httpRequest.getMethod();
		String requestIp = httpRequest.getRemoteAddr();
		
		logVo.setStartTimestamp(System.currentTimeMillis());
		logVo.setUuid(uuid);
		logVo.setBizService(bizService);
		logVo.setHttpMethod(method);
		logVo.setRequestIp(requestIp);
	}
	
	private RestHeaderRequest buildHeaderRequest(HttpServletRequest httpRequest) {
		RestHeaderRequest header = new RestHeaderRequest();
		header.setUuid(httpRequest.getHeader("uuid"));
		header.setBizService(httpRequest.getHeader("bizService"));
		header.setSeriesId(httpRequest.getHeader("seriesId"));
		header.setToken(httpRequest.getHeader("token"));
		
		if ( httpRequest.getRequestURL() == null ) {
			header.setRequestURL(null);
		} else {
			int lastIndex = httpRequest.getRequestURL().lastIndexOf(":");
			header.setRequestURL(httpRequest.getRequestURL().substring(0, lastIndex));
		}
		long timestamp = System.currentTimeMillis();
		try {
			timestamp = Long.parseLong(httpRequest.getHeader("timestamp"));
		} catch (Exception e) {}
		header.setTimestamp(timestamp);
		return header;
	}
	
	private void initLogParam(HttpServletRequest httpRequest) {

		String uuid = UUID.randomUUID().toString();
		uuid = StringUtils.isEmpty(uuid) ? Constant.LOG_PARM_UNKNOWN_VAL : uuid;
		String bizService = httpRequest.getHeader("bizService");
		bizService = StringUtils.isEmpty(bizService) ? Constant.LOG_PARM_UNKNOWN_VAL : bizService;
		String method = httpRequest.getMethod();
		
		ThreadContext.put(Constant.LOG_PARM_KEY_UUID, uuid);
		ThreadContext.put(Constant.LOG_PARM_KEY_BIZ_SERVICE, bizService);
		ThreadContext.put(Constant.LOG_PARM_KEY_HTTP_METHOD, method);
	}
	
	
	private void clearLogParam() {
		
		ThreadContext.remove(Constant.LOG_PARM_KEY_UUID);
		ThreadContext.remove(Constant.LOG_PARM_KEY_BIZ_SERVICE);
		ThreadContext.remove(Constant.LOG_PARM_KEY_HTTP_METHOD);
	}
	
	private String getEncryptData(RestHeaderRequest header, String data) {
		if ( header == null ) {
			return data;
		}
		
		if ( data == null ) {
			return data;
		} else {
			return data;
		}
	}
}
