package com.xplink.api.xmas.controller.vo;

public class PlayerPlyResponseParams {
	
	private Integer no;
	private Integer user_id;
	private String player;
	private String player_stts;
	private String player_keyword;
	private String provider;
	private String provider_keyword;
	private String username;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	public String getPlayer_keyword() {
		return player_keyword;
	}

	public void setPlayer_keyword(String player_keyword) {
		this.player_keyword = player_keyword;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getProvider_keyword() {
		return provider_keyword;
	}

	public void setProvider_keyword(String provider_keyword) {
		this.provider_keyword = provider_keyword;
	}

	public Integer getNo() {
		return no;
	}

	public void setNo(Integer no) {
		this.no = no;
	}

	public String getPlayer() {
		return player;
	}

	public void setPlayer(String player) {
		this.player = player;
	}

	public String getPlayer_stts() {
		return player_stts;
	}

	public void setPlayer_stts(String player_stts) {
		this.player_stts = player_stts;
	}
}
