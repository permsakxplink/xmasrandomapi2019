package com.xplink.api.xmas.controller.vo;

public class UserInfo {

	private Integer userId;
	private String username;
	private String password;
	private String newPassword;
	private String firstName;
	private String lastName;
	private String keywordFlag;

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getKeywordFlag() {
		return keywordFlag;
	}

	public void setKeywordFlag(String keywordFlag) {
		this.keywordFlag = keywordFlag;
	}

	@Override
	public String toString() {
		return "UserInfo [userId=" + userId + ", username=" + username + ", password=" + password + ", newPassword="
				+ newPassword + ", firstName=" + firstName + ", lastName=" + lastName + ", keywordFlag=" + keywordFlag
				+ "]";
	}

}
