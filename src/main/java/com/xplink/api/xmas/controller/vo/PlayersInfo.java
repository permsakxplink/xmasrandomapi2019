package com.xplink.api.xmas.controller.vo;

import java.util.Date;

public class PlayersInfo {

	private Integer userId;
	private String username;
	private String groupId;
	private String groupName;
	private Date groupDttm;
	private Integer groupState;
	private Integer groupCreatedBy;
	private String keyword;
	private Integer userStts;

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Date getGroupDttm() {
		return groupDttm;
	}

	public void setGroupDttm(Date groupDttm) {
		this.groupDttm = groupDttm;
	}

	public Integer getGroupState() {
		return groupState;
	}

	public void setGroupState(Integer groupState) {
		this.groupState = groupState;
	}

	public Integer getGroupCreatedBy() {
		return groupCreatedBy;
	}

	public void setGroupCreatedBy(Integer groupCreatedBy) {
		this.groupCreatedBy = groupCreatedBy;
	}

	public Integer getUserStts() {
		return userStts;
	}

	public void setUserStts(Integer userStts) {
		this.userStts = userStts;
	}
}
