package com.xplink.api.xmas.service;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;

import com.google.gson.Gson;
import com.xplink.api.exception.ServiceException;
import com.xplink.api.log.AbstractLogManager;
import com.xplink.api.rest.constant.ErrorConstant;
import com.xplink.api.rest.controller.vo.GenericRestResponse;
import com.xplink.api.xmas.controller.vo.AuthorizeInfo;
import com.xplink.api.xmas.dao.AuthorizeDao;
import com.xplink.api.xmas.dao.UserDao;
import com.xplink.api.xmas.dao.vo.PersistentLogin;
import com.xplink.api.xmas.dao.vo.User;

@Service
public class AuthorizeService extends AbstractLogManager {

//	private static final int TOKEN_PERIOD_IN_SEC = 600;
	private static final int TOKEN_PERIOD_IN_HOUR = 24;
	
	@Autowired
	private Gson gson;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private AuthorizeDao authorizeDao;
	
//	@Autowired
//	private PersistentTokenBasedRememberMeServices persistentTokenBasedRememberMeServices;
	
//	@Autowired
//	private AuthenticationTrustResolver authenticationTrustResolver;
	
//	private boolean isCurrentAuthenticationAnonymous() {
//		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//		return authenticationTrustResolver.isAnonymous(authentication);
//	}
	
	public String authen(String username, String password) throws ServiceException {
		User user = this.userDao.findByUsername(username);
		if ( user == null ) {
			log_info("User is not found");
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_USER_NOT_FOUND, ErrorConstant.ERR_DESC_USER_NOT_FOUND);
		}
		
		password = new String(Base64Utils.encode(password.getBytes()));
		String dbPassword = user.getPassword();	
		log_debug("DBPassword=" + dbPassword + ", Password=" + password);
		
		if ( !dbPassword.equals(password) ) {
			log_debug("Password is not correct");
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_WRONG_PASS, ErrorConstant.ERR_DESC_WRONG_PASS);
		}
		
		PersistentLogin oldPersistentLogin = this.authorizeDao.getTokenByUsername(username);
		if ( oldPersistentLogin == null ) {
			
			String token = new BCryptPasswordEncoder().encode(username + password + System.currentTimeMillis());
			PersistentLogin persistentLogin = new PersistentLogin();
			persistentLogin.setUsername(username);
			persistentLogin.setSeries(UUID.randomUUID().toString());
			persistentLogin.setToken(token);
			
			Calendar nowDttm = Calendar.getInstance(Locale.US);
//			nowDttm.add(Calendar.SECOND, TOKEN_PERIOD_IN_SEC);
			nowDttm.add(Calendar.HOUR, TOKEN_PERIOD_IN_HOUR);
			persistentLogin.setLastUsed(nowDttm.getTime());
			this.authorizeDao.createNewToken(persistentLogin);
			
			AuthorizeInfo authorizeInfo = new AuthorizeInfo();
			authorizeInfo.setFirstname(user.getFirstName());
			authorizeInfo.setLastname(user.getLastName());
			authorizeInfo.setToken(persistentLogin.getToken());
			authorizeInfo.setSeries(persistentLogin.getSeries());
			authorizeInfo.setRoleName(user.getRoleName());
			authorizeInfo.setUserId(user.getUserId());
			
			GenericRestResponse<AuthorizeInfo> response = new GenericRestResponse<AuthorizeInfo>();
			response.setData(authorizeInfo);
			return this.gson.toJson(response);
		}
		
		Calendar nowDttm = Calendar.getInstance(Locale.US);
		Date now = nowDttm.getTime();
		Date lastUsed = oldPersistentLogin.getLastUsed();
		log_info("LastUsed={0,date,yyyy-MM-dd HH:mm:ss.SSS}, Now={1,date,yyyy-MM-dd HH:mm:ss.SSS}", lastUsed, now);
		if ( lastUsed.before(now) ) {
//			nowDttm.add(Calendar.SECOND, TOKEN_PERIOD_IN_SEC);
			nowDttm.add(Calendar.HOUR, TOKEN_PERIOD_IN_HOUR);
			String token = new BCryptPasswordEncoder().encode(username + password + System.currentTimeMillis());
			oldPersistentLogin.setToken(token);
			oldPersistentLogin.setLastUsed(nowDttm.getTime());
			this.authorizeDao.updateToken(oldPersistentLogin.getSeries(), oldPersistentLogin.getToken(), oldPersistentLogin.getLastUsed());
		}
		
		AuthorizeInfo authorizeInfo = new AuthorizeInfo();
		authorizeInfo.setToken(oldPersistentLogin.getToken());
		authorizeInfo.setSeries(oldPersistentLogin.getSeries());
		authorizeInfo.setRoleName(oldPersistentLogin.getRoleName());
		authorizeInfo.setFirstname(user.getFirstName());
		authorizeInfo.setLastname(user.getLastName());
		authorizeInfo.setUserId(user.getUserId());
		
		GenericRestResponse<AuthorizeInfo> response = new GenericRestResponse<AuthorizeInfo>();
		response.setData(authorizeInfo);
		return this.gson.toJson(response);
	}
	
	public void updateToken(String seriesId, String inToken) throws ServiceException {
		log_info("updateToken in AuthorizeService");
		PersistentLogin oldPersistentLogin = this.authorizeDao.getTokenForSeries(seriesId);
		log_info("seriesId=" + seriesId);
		if ( oldPersistentLogin == null ) {
			log_debug("Existing PersistentLogin is not found");
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_USER_EXPR_OR_TOKEN_NOT_FOUND, ErrorConstant.ERR_DESC_USER_EXPR_OR_TOKEN_NOT_FOUND);
		}
		
		Calendar nowDttm = Calendar.getInstance(Locale.US);
		Date now = nowDttm.getTime();
		Calendar lastUsedDttm = Calendar.getInstance(Locale.US);
		Date lastUsed = oldPersistentLogin.getLastUsed();
		lastUsedDttm.setTime(lastUsed);
		log_info("LastUsed={0,date,yyyy-MM-dd HH:mm:ss.SSS}, Now={1,date,yyyy-MM-dd HH:mm:ss.SSS}", lastUsed, now);
		if ( lastUsed.before(now) ) {
			log_debug("Token has been expired");
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_USER_EXPR_OR_TOKEN_NOT_FOUND, ErrorConstant.ERR_DESC_USER_EXPR_OR_TOKEN_NOT_FOUND);
		}
		
		String oldToken = oldPersistentLogin.getToken();	
		if (oldToken.equals(inToken) ) {
			
			long period = (lastUsed.getTime() - now.getTime()) / 1000;
//			log_debug("PeriodTime=" + period + ", TokenPeriodTime=" + TOKEN_PERIOD_IN_SEC);
//			if ( period > TOKEN_PERIOD_IN_SEC ) {
			log_debug("PeriodTime=" + period + ", TokenPeriodTime=" + TOKEN_PERIOD_IN_HOUR);
				if ( period > TOKEN_PERIOD_IN_HOUR ) {
			return;
			}
		
//			lastUsedDttm.add(Calendar.SECOND, TOKEN_PERIOD_IN_SEC);
			lastUsedDttm.add(Calendar.HOUR, TOKEN_PERIOD_IN_HOUR);
			oldPersistentLogin.setLastUsed(lastUsedDttm.getTime());
			this.authorizeDao.updateToken(seriesId, oldPersistentLogin.getToken(), oldPersistentLogin.getLastUsed());
		}else {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_INVD_TOKEN, ErrorConstant.ERR_DESC_INVD_TOKEN);
		}
	}
	
	public String deleteByUsername(String username) {
		this.authorizeDao.removeUserTokens(username);
		return this.gson.toJson(new GenericRestResponse<String>());
	}
	
//	
//	public void logout(HttpServletRequest request, HttpServletResponse response) {
//		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//		if (auth != null) {
//			new SecurityContextLogoutHandler().logout(request, response, auth);
////			persistentTokenBasedRememberMeServices.logout(request, response, auth);
//			SecurityContextHolder.getContext().setAuthentication(null);
//		}
//	}
	
}
