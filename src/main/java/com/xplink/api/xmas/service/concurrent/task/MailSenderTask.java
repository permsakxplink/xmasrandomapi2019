package com.xplink.api.xmas.service.concurrent.task;

import java.io.Serializable;

import javax.mail.internet.MimeMessage;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.xplink.api.log.AbstractLogManager;
import com.xplink.api.rest.constant.Constant;
import com.xplink.api.rest.constant.ErrorConstant;

public class MailSenderTask extends AbstractLogManager implements Runnable,  Serializable {

	private static final long serialVersionUID = 5421027506222778148L;

	private static final String SRVC_NAME = MailSenderTask.class.getSimpleName();
	
	private JavaMailSender mailSender;
	
	private String mailFrom;
	private String mailSubject;
	private String mailTo;
	private String message;
	
	private String uuid;
	private String bizService;
	private String method;

	public MailSenderTask(JavaMailSender mailSender, String mailFrom, String mailTo, String mailSubject, String message, String uuid, String bizService, String method) {
		this.uuid = uuid;
		this.bizService = bizService;
		this.method = method;
		
		this.mailFrom = mailFrom;
		this.mailTo = mailTo;
		this.mailSubject = mailSubject;
		this.mailSender = mailSender;
		this.message = message;
	}
	
	@Override
	public void run() {
		long startTime = System.currentTimeMillis();
		ThreadContext.put(Constant.LOG_PARM_KEY_UUID, this.uuid);
		ThreadContext.put(Constant.LOG_PARM_KEY_BIZ_SERVICE, this.bizService);
		ThreadContext.put(Constant.LOG_PARM_KEY_HTTP_METHOD, this.method);
		ThreadContext.put("threadId", "[" + SRVC_NAME + "-" + Thread.currentThread().getId() +"]");
		log_info("Start execute " + SRVC_NAME);
		try {

			log_debug("Mail From=" + this.mailFrom + ", To=" + this.mailTo + ", Subject=" + this.mailSubject +" , Message=" + this.message);
			MimeMessage message = mailSender.createMimeMessage();
			
			MimeMessageHelper helper = new MimeMessageHelper(message, "UTF-8");
			helper.setFrom(this.mailFrom, "Christmas Random 2019");
			helper.setTo(this.mailTo);
			helper.setSubject(this.mailSubject);
			helper.setText(this.message, true);

			this.mailSender.send(message);
			
		} catch (Throwable e) {
			String errorMessage = ErrorConstant.ERR_DESC_CANNOT_EXEC_TASK + ", error=, " + e.getMessage();
			log_error("SERVICE_NAME:" + SRVC_NAME + "|ERRCODE:" + ErrorConstant.ERR_CODE_CANNOT_EXEC_TASK + "|ERRMESSAGE:" + errorMessage + "|workingTime = " + (System.currentTimeMillis() - startTime), e);
		} finally {
			log_info("End execute " + SRVC_NAME + ", elapsed time " + (System.currentTimeMillis() - startTime) + " ms");
			ThreadContext.remove(Constant.LOG_PARM_KEY_UUID);
			ThreadContext.remove(Constant.LOG_PARM_KEY_BIZ_SERVICE);
			ThreadContext.remove(Constant.LOG_PARM_KEY_HTTP_METHOD);
			ThreadContext.remove("threadId");
		}
	}

}
