package com.xplink.api.xmas.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.coyote.Processor;
import org.codehaus.jackson.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.xplink.api.exception.ServiceException;
import com.xplink.api.log.AbstractLogManager;
import com.xplink.api.rest.constant.ErrorConstant;
import com.xplink.api.rest.controller.vo.GenericRestResponse;
import com.xplink.api.xmas.controller.vo.DelEventInfo;
import com.xplink.api.xmas.controller.vo.EventInfo;
import com.xplink.api.xmas.controller.vo.PlayerAllResponse;
import com.xplink.api.xmas.controller.vo.PlayerPlyResponseParams;
import com.xplink.api.xmas.controller.vo.PlayerReqsResponseParams;
import com.xplink.api.xmas.controller.vo.PlayersInfo;
import com.xplink.api.xmas.dao.AuthorizeDao;
import com.xplink.api.xmas.dao.EventDao;
import com.xplink.api.xmas.dao.GroupDao;
import com.xplink.api.xmas.dao.UserDao;
import com.xplink.api.xmas.dao.vo.Event;
import com.xplink.api.xmas.dao.vo.Group;
import com.xplink.api.xmas.dao.vo.PersistentLogin;
import com.xplink.api.xmas.dao.vo.User;

@Service
public class EventService extends AbstractLogManager {

	@Autowired
	private Gson gson;

	@Autowired
	private EventDao eventDao;

	@Autowired
	private UserDao userDao;

	@Autowired
	private GroupDao groupDao;
	
	@Autowired
	AuthorizeDao authorizeDao;
	
	@Value("${mail.xmas.from}")
	private String mailFrom;

	@Value("${mail.xmas.subject}")
	private String mailSubject;
	
	@Value("${app.conf.dir}")
	private String confDir;
	
	public String showKeyword(String groupName, String token) throws ServiceException {
		PersistentLogin persist = this.authorizeDao.findRoleAdmin(token); 
		Group group = this.groupDao.findByGroupName(groupName);
		Event event = this.eventDao.findByGroupId(group.getGroupId(), persist.getUserId());
		log_debug("event : " + event.getEvtReceiveKeyword());
		EventInfo eventInfo = new EventInfo();
		if ( group.getGroupState() == 0 ) {
			eventInfo.setKeyword(event.getEvtKeyword() == null ? null : new String(Base64Utils.decode(event.getEvtKeyword().getBytes())));
			eventInfo.setReceive_keyword(event.getEvtReceiveKeyword() == null ? null : 
				new String(Base64Utils.decode(event.getEvtReceiveKeyword().getBytes())));
		} else if ( group.getGroupState() == 1 || group.getGroupState() == 2 ) {
			eventInfo.setKeyword(event.getEvtKeyword() == null ? null : new String(Base64Utils.decode(event.getEvtKeyword().getBytes())));
			eventInfo.setReceive_keyword(event.getEvtReceiveKeyword() == null ? null : 
				new String(Base64Utils.decode(event.getEvtReceiveKeyword().getBytes())));
		} else {
			return null;
		}
		
		GenericRestResponse<EventInfo> response = new GenericRestResponse<EventInfo>();
		response.setData(eventInfo);
		return this.gson.toJson(response);
	}
	
//	public static void main(String...args) {
//		String test = new String(Base64Utils.decode("dGVzdC1rZXl3b3JkLTQ=".getBytes()));
//		System.out.print(test);
//	}

	public String findPlayersByGroupId(String groupName, String token) throws ServiceException {
		
		PersistentLogin persist = this.authorizeDao.findRoleAdmin(token);
		Group group = this.groupDao.findByGroupName(groupName);
		if ( group == null ) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_GROUP_NOT_FOUND, ErrorConstant.ERR_DESC_GROUP_NOT_FOUND);
		}
		Event event = this.eventDao.findByGroupId(group.getGroupId(), persist.getUserId());
		if (event == null && !"ADMIN".equals(persist.getRoleName()) ) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_ACCESS_DENIED, ErrorConstant.ERR_DESC_ACCESS_DENIED);
		}
		
		List<PlayersInfo> list = this.eventDao.findPlayersByGroup(group.getGroupId());
		if (list == null) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_EVENT_NOT_FOUND_USER, ErrorConstant.ERR_CODE_EVENT_NOT_FOUND_USER);
		}
			
		List<PlayerPlyResponseParams> listPlays = new ArrayList<PlayerPlyResponseParams>();
		List<PlayerReqsResponseParams> listReqs = new ArrayList<PlayerReqsResponseParams>();
		int a = 1; int b = 1;
		for ( PlayersInfo playersInfo : list ) {
//			if (playersInfo.getUserStts() == 1) {
				
				PlayerPlyResponseParams plays = new PlayerPlyResponseParams();
				User user = this.userDao.findById(playersInfo.getUserId());
				plays.setNo(a++);
				plays.setUser_id(user.getUserId());
				plays.setUsername(user.getUsername());
				plays.setPlayer(user.getFirstName()+ " " +user.getLastName());
				if (playersInfo.getKeyword() == null) {
					plays.setPlayer_stts("no");
				} else {
					plays.setPlayer_stts("yes");
				}
				
//				event = this.eventDao.findByGroupId(group.getGroupId(), playersInfo.getUserId());
//				if (event.getEvtReceiveKeyword() == null) {
//					plays.setPlayer_keyword(null);
//					plays.setProvider_keyword(null);
//					plays.setProvider(null);
//				} else {
////					plays.setPlayer_keyword(event.getEvtKeyword() == null ? null : new String(Base64Utils.decode(event.getEvtKeyword().getBytes())));
//					plays.setProvider_keyword(event.getEvtReceiveKeyword() == null ? null : new String(Base64Utils.decode(event.getEvtReceiveKeyword().getBytes())));
//					event = this.eventDao.findByKeyword(group.getGroupId(), event.getEvtReceiveKeyword());
//					user = this.userDao.findById(event.getUserId());
//					plays.setProvider(user.getFirstName()+ " " +user.getLastName());
//				}
	
				listPlays.add(plays);
				
//			} 
//			else if (playersInfo.getUserStts() == 0) {
//				PlayerReqsResponseParams reqs = new PlayerReqsResponseParams();
//				User user = this.userDao.findById(playersInfo.getUserId());
//				reqs.setNo(b++);
//				reqs.setUser_id(user.getUserId());
//				reqs.setUsername(user.getUsername());
//				reqs.setPlayer(user.getFirstName()+ " " +user.getLastName());		
//				listReqs.add(reqs);
//			}
		}
		
		User user = this.userDao.findById(list.get(0).getGroupCreatedBy());
		PlayerAllResponse playersResponse = new PlayerAllResponse();	
		for ( PlayersInfo playersInfo : list ) {
			if ( playersInfo.getKeyword() == null && group.getGroupState() == 0 ) {
				playersResponse.setKeyword_status("keyword_null");
				break;
			} else {
				playersResponse.setKeyword_status("keyword_ready");
			}
		}
		
		if ( group.getGroupState() == 0 ){
			playersResponse.setGroup_status("ready");
		} else if( group.getGroupState() == 1 ) {
			playersResponse.setGroup_status("started");
		} else if( group.getGroupState() == 2 ) {
			playersResponse.setGroup_status("finished");
		}
		log_debug(">>>>>>>>>>user = {0}", this.gson.toJson(user));
		playersResponse.setCreated_by(user.getFirstName() == null ? null : user.getFirstName() + " " + user.getLastName() == null ? null : user.getLastName());
		playersResponse.setCreated_at(formatTime(group.getGroupDttm()));
		playersResponse.setCreated_by_id(group.getGroupCreatedBy());
		playersResponse.setDescription(group.getDescription());
		playersResponse.setRoom_name(group.getGroupName());
		playersResponse.setPlayers(listPlays);
		playersResponse.setRequests(listReqs);

		GenericRestResponse<PlayerAllResponse> response = new GenericRestResponse<PlayerAllResponse>();
		response.setData(playersResponse);
		
		return this.gson.toJson(response);
	}
	
	public String formatTime(Date date) {
		SimpleDateFormat formatTime = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
		return formatTime.format(date);
	}

	public void validationGroupStatus(Group group) throws ServiceException {
		if (group == null) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_GROUP_NOT_FOUND, ErrorConstant.ERR_DESC_GROUP_NOT_FOUND);
		}
		if (group.getGroupState() == 1) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_GROUP_STARTED, ErrorConstant.ERR_DESC_GROUP_STARTED);
		}
		if (group.getGroupState() == 2) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_GROUP_FINISHED, ErrorConstant.ERR_DESC_GROUP_FINISHED);
		}
	}
	
	public String update(EventInfo eventInfo, String token) throws ServiceException {
		PersistentLogin persist = this.authorizeDao.findRoleAdmin(token);
		Group group = this.groupDao.findByGroupName(eventInfo.getGroupName());
		validationGroupStatus(group);
		log_debug("{0} /// {1}",group.getGroupId(), persist.getUserId());
		Event event = eventDao.findByGroupId(group.getGroupId(), persist.getUserId());
		if (event == null) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_ACCESS_DENIED, ErrorConstant.ERR_DESC_ACCESS_DENIED);
		}
		log_debug("{0} ||| {1}",group.getGroupId(), eventInfo.getKeyword());
		event = eventDao.findByKeyword(group.getGroupId(), eventInfo.getKeyword());
		if (event != null) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_EVENT_KEYWORD_DUPLICATE, ErrorConstant.ERR_DESC_EVENT_KEYWORD_DUPLICATE);
		}
		
		event = new Event();
		event.setGroupId(group.getGroupId());
		event.setUserId(persist.getUserId());
		event.setEvtKeyword(new String(Base64Utils.encode(eventInfo.getKeyword().getBytes())));
		if (eventInfo.getReceive_keyword() != null) {
			event.setEvtReceiveKeyword(new String(Base64Utils.encode(eventInfo.getReceive_keyword().getBytes())));
		}
		event.setEvtLastChngDttm(Calendar.getInstance(Locale.US).getTime());
		eventDao.update(event);

		return this.gson.toJson(new GenericRestResponse<String>());
	}

	/* ADMIN ...called by GroupService.deleteById */
	public String deleteByGroupId(EventInfo eventInfo) {
		Event event = this.eventDao.findById(eventInfo.getEvtId());
		if (event != null) {
			this.eventDao.deleteByGroupId(event.getGroupId());
		}
		return this.gson.toJson(new GenericRestResponse<String>());
	}

	public String deleteByUserId(DelEventInfo delEventInfo) {
		Event info = new Event();
		info.setGroupId(delEventInfo.getGroupId());
		info.setUserId(delEventInfo.getUserId());
		Event event = eventDao.findByGroupEventId(info);
		if (event != null) {
			User user = userDao.findById(delEventInfo.getLoginUserId());
			Group group = groupDao.findById(event.getGroupId());
			log_info("userId=" + user.getUserId() + ", createdId=" + group.getGroupCreatedBy());
			if (user.getUserId() == group.getGroupCreatedBy() && group.getGroupState() == 0 && user.getUserId() != delEventInfo.getUserId()) {
				EventInfo eventInfo = new EventInfo();
				eventInfo.setGroupId(info.getGroupId());
				eventInfo.setUserId(info.getUserId());
				this.eventDao.deleteByUserId(eventInfo);
			}
		}
		return this.gson.toJson(new GenericRestResponse<String>());
	}

	public String exitGroup(Integer userId, String groupName, String token) throws ServiceException {
		
		PersistentLogin persist = this.authorizeDao.findRoleAdmin(token);
		Group group = groupDao.findByGroupName(groupName);
		if ( group == null ) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_GROUP_NOT_FOUND, ErrorConstant.ERR_DESC_GROUP_NOT_FOUND);
		}
		
		User user = this.userDao.findById(userId);
		if ( user == null ) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_USER_NOT_FOUND, ErrorConstant.ERR_DESC_USER_NOT_FOUND);
		} else if (user.getUserId() != persist.getUserId() && persist.getUserId() != group.getGroupCreatedBy()) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_YOU_CANNOT_DELETE, ErrorConstant.ERR_DESC_YOU_CANNOT_DELETE);
		}

		if (user.getUserId() == group.getGroupCreatedBy()) {
			EventInfo eventInfo = new EventInfo();
			eventInfo.setGroupId(group.getGroupId());
			eventInfo.setUserId(user.getUserId());
			this.eventDao.deleteByUserId(eventInfo);
			
			Event event = this.eventDao.findByGroupLimit(group.getGroupId());
			if ( event != null ) { 
				this.groupDao.updateCreateBy(event.getGroupId(), event.getUserId());
			} else {
				this.groupDao.deleteById(group.getGroupId());
			}
		} else {
			EventInfo eventInfo = new EventInfo();
			eventInfo.setGroupId(group.getGroupId());
			eventInfo.setUserId(user.getUserId());
			this.eventDao.deleteByUserId(eventInfo);
		}
		return this.gson.toJson(new GenericRestResponse<String>());
	}
}
