package com.xplink.api.xmas.service;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;
import com.xplink.api.exception.ServiceException;
import com.xplink.api.log.AbstractLogManager;
import com.xplink.api.rest.constant.ErrorConstant;
import com.xplink.api.rest.controller.vo.GenericRestResponse;
import com.xplink.api.rest.controller.vo.RestHeaderRequest;
import com.xplink.api.xmas.captcha.CaptchaRequest;
import com.xplink.api.xmas.controller.vo.Captcha;
import com.xplink.api.xmas.controller.vo.CaptchaInfo;
import com.xplink.api.xmas.controller.vo.UserInfo;
import com.xplink.api.xmas.dao.AuthorizeDao;
import com.xplink.api.xmas.dao.UserDao;
import com.xplink.api.xmas.dao.vo.PersistentLogin;
import com.xplink.api.xmas.dao.vo.User;

@Service
public class UserService extends AbstractLogManager {

	@Autowired
	private Gson gson;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private AuthorizeDao authorizeDao;

	public String findAllUsers() {
		List<User> list = this.userDao.findAllUsers();
		
		List<UserInfo> userInfoList = new ArrayList<UserInfo>();
		for (User user : list) {
			UserInfo userInfo = new UserInfo();
			userInfo.setUserId(user.getUserId());
			userInfo.setUsername(user.getUsername());
			userInfo.setFirstName(user.getFirstName());
			userInfo.setLastName(user.getLastName());
			userInfoList.add(userInfo);
		}
		
		GenericRestResponse<List<UserInfo>> response = new GenericRestResponse<List<UserInfo>>();
		response.setData(userInfoList);
		return this.gson.toJson(response);
	}
	
	public String findUserById(Integer userId) throws ServiceException {
		User user = this.userDao.findById(userId);
		if ( user == null ) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_USER_NOT_FOUND, ErrorConstant.ERR_DESC_USER_NOT_FOUND);
		}
		
		UserInfo userInfo = new UserInfo();
		userInfo.setUserId(user.getUserId());
		userInfo.setUsername(user.getUsername());
		userInfo.setFirstName(user.getFirstName());
		userInfo.setLastName(user.getLastName());
		
		GenericRestResponse<UserInfo> response = new GenericRestResponse<UserInfo>();
		response.setData(userInfo);
		return this.gson.toJson(response);
	}
	
	public String acceptCreateByEmail(String recaptcha, UserInfo userInfo, RestHeaderRequest header) throws ServiceException {
		User user = this.userDao.findByUsername(userInfo.getUsername());	
		
		if ( user != null ) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_USER_DUPLICATE, ErrorConstant.ERR_DESC_USER_DUPLICATE);
		}
		
		user = this.userDao.findByFullName(userInfo.getFirstName(), userInfo.getLastName());
		
		if ( user != null ) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_USER_FULL_NAME_DUPLICATE, ErrorConstant.ERR_DESC_USER_FULL_NAME_DUPLICATE);
		}
		
		if (StringUtils.isEmpty(userInfo.getUsername()) || StringUtils.isEmpty(userInfo.getPassword()) || StringUtils.isEmpty(userInfo.getFirstName()) || StringUtils.isEmpty(userInfo.getLastName())) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_UNEXPECTED, ErrorConstant.ERR_DESC_UNEXPECTED);
		}
		
		CaptchaInfo captchaInfo = new CaptchaInfo();
		captchaInfo.setResponse(recaptcha);
		captchaInfo.setRemoteip(header.getRemoteHost());
		
		Captcha captcha = new Captcha();
		try {
			CaptchaRequest captchaRequest = new CaptchaRequest();
			captcha = captchaRequest.CapchaCalling(captchaInfo);
		} catch (Exception e) {
			log_info(" Exception Captcha : " +e);
		} 
		try {
			this.emailService.acceptRegister(userInfo, header);
		} catch (NumberFormatException | UnknownHostException e) {
			log_info(e.getMessage());
		}
		
//		if ( captcha.isSuccess() == false ) {
//			log_info("Data captcha false : " +captcha.toString());
//			Map<String,String> mapError = new HashMap<String, String>();
//			mapError.put("missing-input-secret", "The secret parameter is missing");
//			mapError.put("invalid-input-secret", "The secret parameter is invalid or malformed");
//			mapError.put("missing-input-response", "The response parameter is missing");
//			mapError.put("invalid-input-secret", "The response parameter is invalid or malformed");
//			mapError.put("bad-request", "The request is invalid or malformed");
//			
//			String errorMessage = mapError.get(captcha.getErrorCodes().get(0));
//			if (captcha.getErrorCodes().size() > 1) {
//				for (int i=1; i<captcha.getErrorCodes().size(); i++) {
//					errorMessage = errorMessage+ ", " +mapError.get(captcha.getErrorCodes().get(i));
//				}
//				errorMessage = errorMessage+ ".";
//			}
//			log_info("Capcha error : " +errorMessage);
//			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_CAPTCHA_ERROR, ErrorConstant.ERR_DESC_CAPTCHA_ERROR);
//		
//		} else if ( captcha.isSuccess() == true ) {
//			try {
//				this.emailService.acceptRegister(userInfo, header);
//			} catch (NumberFormatException | UnknownHostException e) {
//				log_info(e.getMessage());
//			}
//	}  else {
//			return null;
//		} 
		
		return this.gson.toJson(new GenericRestResponse<String>());
	}
	
	public String create(UserInfo userInfo) throws ServiceException {
		User user = this.userDao.findByUsername(userInfo.getUsername());	
		
		if( user != null ) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_USER_DUPLICATE, ErrorConstant.ERR_DESC_USER_DUPLICATE);
		}
		
		user = new User();
		user.setUsername(userInfo.getUsername());
		user.setPassword(new String(Base64Utils.encode(userInfo.getPassword().getBytes())));
		user.setFirstName(userInfo.getFirstName());
		user.setLastName(userInfo.getLastName());
		user.setLastChngDttm(Calendar.getInstance(Locale.US).getTime());
	
		int id = this.userDao.create(user);
		user.setUserId(id);
		userInfo.setUserId(user.getUserId());
		
		return this.gson.toJson(new GenericRestResponse<String>());
	}
	
	public String update(UserInfo userInfo, String token) throws ServiceException {
		PersistentLogin persist = this.authorizeDao.findRoleAdmin(token);	
		User user = this.userDao.findById(persist.getUserId());
		if ( user == null ) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_USER_NOT_FOUND, ErrorConstant.ERR_DESC_USER_NOT_FOUND);
		}	
		
		User userCheckDup = this.userDao.findByFullName(userInfo.getFirstName(), userInfo.getLastName());
		
		if ( userCheckDup != null && persist.getUserId()!= userCheckDup.getUserId()) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_USER_FULL_NAME_DUPLICATE, ErrorConstant.ERR_DESC_USER_FULL_NAME_DUPLICATE);
		}
		
		String firstName = userInfo.getFirstName() == null ? user.getFirstName() : userInfo.getFirstName();
		String lastName = userInfo.getLastName() == null ? user.getLastName() : userInfo.getLastName();	
		String oldPassword = userInfo.getPassword() == null ? user.getPassword() : new String(Base64Utils.encode(userInfo.getPassword().getBytes()));
		String newPassword = userInfo.getNewPassword() == null ? user.getPassword() : new String(Base64Utils.encode(userInfo.getNewPassword().getBytes()));
		
		if (!user.getPassword().equals(oldPassword)) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_INVD_OLD_PASSWORD, ErrorConstant.ERR_DESC_INVD_OLD_PASSWORD);
		}

		user.setPassword(newPassword);
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setLastChngDttm(Calendar.getInstance(Locale.US).getTime());
		log_debug("user = " +user.toString());
		this.userDao.update(user);

		return this.gson.toJson(new GenericRestResponse<String>());
	}
	
	public String delete(Integer userId, String token) throws ServiceException {
		PersistentLogin persist = this.authorizeDao.findRoleAdmin(token);
		
		if (!"ADMIN".equals(persist.getRoleName())) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_YOU_CANNOT_DELETE, ErrorConstant.ERR_DESC_YOU_CANNOT_DELETE);
		}
		
		User user = this.userDao.findById(userId);
		if ( user != null && "ADMIN".equals(user.getRoleName())) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_CANNOT_DELETE_ADMIN, ErrorConstant.ERR_DESC_CANNOT_DELETE_ADMIN);
		}
		if ( user != null ) {
			try {
			this.userDao.deleteById(userId);
			}catch(Exception e){
				throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_NOT_DELETE_USER_EVENT, ErrorConstant.ERR_DESC_NOT_DELETE_USER_EVENT);
			}
		}
		
		return this.gson.toJson(new GenericRestResponse<String>());
	}

	public String forgetPassword(RestHeaderRequest header, UserInfo userInfo) throws ServiceException {

		User user = this.userDao.findByUsername(userInfo.getUsername());
		if (user == null) {
			try {
				return this.emailService.inviteRegister(header, userInfo);
			} catch (NumberFormatException e) {
				log_info(e.getMessage());
			}
		} else {
			try {
				return this.emailService.forgetPasswordEmail(header, user);
			} catch (NumberFormatException e) {
				log_info(e.getMessage());
			}
		}

		return null;
	}
}
