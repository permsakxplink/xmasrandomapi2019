package com.xplink.api.xmas.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xplink.api.log.AbstractLogManager;
import com.xplink.api.xmas.dao.UserDao;
import com.xplink.api.xmas.dao.vo.User;

@Service("customUserDetailsService")
public class CustomUserDetailsService extends AbstractLogManager implements UserDetailsService {

	@Autowired
	private UserDao userDao;
	
	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userDao.findByUsername(username);
		logger.info("User : {}", user);
		if (user == null) {
			logger.info("User not found");
			throw new UsernameNotFoundException("Username not found");
		}
		
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), true, true,
				true, true, getGrantedAuthorities(user));
	}
	
	private List<GrantedAuthority> getGrantedAuthorities(User user) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
//		for (M2MRole role : user.getRoleSet()) {
//			logger.info("Role User : {}", role.getRoleName());
//			for (M2MPerm perm : role.getPermSet()) {
//				logger.info("Role Permission : {}", perm.getPermDesc());
//				authorities.add(new SimpleGrantedAuthority("ROLE_" + perm.getPermCode()));
//			}
//		}
		logger.info("authorities : {}", authorities);
		return authorities;
	}


}
