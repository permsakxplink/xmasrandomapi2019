package com.xplink.api.xmas.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.logging.log4j.Logger;

public class KeywordRandomUtil {

	//private static Logger logger = LogManager.getLogger(KeywordRandomUtil.class.getClass());
	
	public static List<String> random(List<String> keywordList, Logger logger) {
		
		List<String> randomKeywordList = new ArrayList<String>(keywordList);
		
		int min = 1; int max = 50;
		int randomNum = ThreadLocalRandom.current().nextInt(min, max + 1);

		//logger.debug("Before=" + Arrays.toString(keywordList.toArray()));
		
		
		Collections.shuffle(randomKeywordList, new Random(randomNum));
		
		//logger.debug("Shuffle=" + Arrays.toString(randomKeywordList.toArray()));
		
		swapPositionInList(keywordList, randomKeywordList, logger);
		
		//logger.debug("Swap=" + Arrays.toString(randomKeywordList.toArray()));
		return randomKeywordList;
	}
	
	private static void swapPositionInList(List<String> keywordList, List<String> randKeywordList, Logger logger) {

		int size = keywordList.size();
		int positionToSwap = 0;
		for(int i = 0; i < size; i++) {
			
			String keyword = keywordList.get(i);
			String randKey = randKeywordList.get(i);
			
			if(keyword.equals(randKey)) {
			
				if(i == 0) {
					
					positionToSwap++;
				}
				
				//logger.debug("Going to swap position #" + i + " and position #" + positionToSwap);
				Collections.swap(randKeywordList, positionToSwap, i);
				positionToSwap = 0;
			}
		}
	}
}
