package com.xplink.api.xmas.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.xplink.api.xmas.dao.vo.PersistentLogin;

public class PersistentLoginRowMapper implements RowMapper<PersistentLogin> {

	@Override
	public PersistentLogin mapRow(ResultSet rs, int rowNum) throws SQLException {
		PersistentLogin model = new PersistentLogin();
		model.setUsername(rs.getString("username"));
		model.setSeries(rs.getString("series"));
		model.setToken(rs.getString("token"));
		model.setLastUsed(rs.getTimestamp("last_used"));
		model.setUserId(rs.getInt("usr_id"));
		model.setRoleName(rs.getString("role_name"));
		return model;
	}

}
