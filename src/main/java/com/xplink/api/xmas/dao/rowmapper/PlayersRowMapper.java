package com.xplink.api.xmas.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.xplink.api.xmas.controller.vo.PlayersInfo;

public class PlayersRowMapper implements RowMapper<PlayersInfo> {
	@Override
	public PlayersInfo mapRow(ResultSet rs, int rowNum) throws SQLException {
		PlayersInfo playersInfo = new PlayersInfo();
		playersInfo.setUserId(rs.getInt("usr_id"));
		playersInfo.setUsername(rs.getString("username"));
		playersInfo.setGroupId(rs.getString("grp_id"));
		playersInfo.setGroupName(rs.getString("grp_name"));
		playersInfo.setGroupState(rs.getInt("grp_state"));
		playersInfo.setGroupCreatedBy(rs.getInt("grp_createdby"));
		playersInfo.setGroupDttm(rs.getDate("grp_dttm"));
		playersInfo.setKeyword(rs.getString("evt_kywd"));
//		playersInfo.setUserStts(rs.getInt("xmas_user_stts"));
		return playersInfo;
	}
}
