package com.xplink.api.xmas.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Arrays;
import java.util.List;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xplink.api.config.sql.SQLPropertyFileConfig;
import com.xplink.api.dao.AbstractDao;
import com.xplink.api.rest.constant.SQLConstant;
import com.xplink.api.xmas.controller.vo.GroupAllReqs;
import com.xplink.api.xmas.dao.rowmapper.GroupAllRowMapper;
import com.xplink.api.xmas.dao.rowmapper.GroupCountRowMapper;
import com.xplink.api.xmas.dao.rowmapper.GroupRowMapper;
import com.xplink.api.xmas.dao.vo.Group;

@Repository
@Transactional
public class GroupDao extends AbstractDao {

	public int create(Group group) {
		log_info("Create Group = " + group.getGroupName());
		String sql = this.sqlConfig.getSql(SQLConstant.INSERT_GROUP_KEY);
		log_info(SQLConstant.INSERT_GROUP_KEY + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));

		KeyHolder keyHolder = new GeneratedKeyHolder();
		int rowUpdate = getJdbcTemplate().update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int i = 1;
				ps.setString(i++, group.getGroupName());
				ps.setTimestamp(i++, new Timestamp(group.getGroupDttm().getTime()));
				ps.setInt(i++, group.getGroupState());
				ps.setInt(i++, group.getGroupCreatedBy());
				return ps;
			}
		}, keyHolder);

		int id = keyHolder.getKey().intValue();
		log_info("Inserted " + rowUpdate + " row(s), id=" + id);
		return id;
	}

	public void update(Group group) {
		log_info("Update Group=" + group.getGroupName());
		String sql = this.sqlConfig.getSql(SQLConstant.UPDATE_GROUP_KEY);
		log_info(SQLConstant.UPDATE_GROUP_KEY + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));

		Object[] parms = { group.getGroupName(), group.getGroupDttm(), group.getGroupState(),
				group.getGroupId() };
		int[] types = { Types.VARCHAR, Types.TIMESTAMP, Types.INTEGER, Types.INTEGER };
		int rowUpdate = getJdbcTemplate().update(sql, parms, types);
		log_info("Updated " + rowUpdate + " row(s)");
	}
	
	public void updateCreateBy(Integer groupId, Integer userId) {
		log_info("Update CreateBy");
		String sql = this.sqlConfig.getSql(SQLConstant.UPDATE_GROUP_CREATE_BY);
		log_info(SQLConstant.UPDATE_GROUP_CREATE_BY + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));

		Object[] parms = { userId, groupId};
		int[] types = { Types.INTEGER, Types.INTEGER };
		int rowUpdate = getJdbcTemplate().update(sql, parms, types);
		log_info("Updated " + rowUpdate + " row(s)");
	}
	
	public void updateState(Integer groupId, Integer state) {
		log_info("Update CreateBy");
		String sql = this.sqlConfig.getSql(SQLConstant.UPDATE_GROUP_STATE);
		log_info(SQLConstant.UPDATE_GROUP_STATE + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));

		Object[] parms = { state, groupId};
		int[] types = { Types.INTEGER, Types.INTEGER };
		int rowUpdate = getJdbcTemplate().update(sql, parms, types);
		log_info("Updated " + rowUpdate + " row(s)");
	}
	
	public void updateGroupNote(Integer groupId, String desc) {
		log_info("Update GroupNote");
		String sql = this.sqlConfig.getSql(SQLConstant.UPDATE_GROUP_NOTE);
		log_info(SQLConstant.UPDATE_GROUP_NOTE + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));

		Object[] parms = { desc, groupId};
		int rowUpdate = getJdbcTemplate().update(sql, parms);
		log_info("Updated " + rowUpdate + " row(s)");
	}

	public Group findByGroupName(String groupName) {
		log_info("GroupName=" + groupName);
		try {
			String sql = this.sqlConfig.getSql(SQLConstant.SELECT_GROUP_BY_NAME_KEY);
			log_info(SQLConstant.SELECT_GROUP_BY_NAME_KEY + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));

			Object[] parms = { groupName };
			int[] types = { Types.VARCHAR };

			return getJdbcTemplate().queryForObject(sql, parms, types, new GroupAllRowMapper());
		} catch (EmptyResultDataAccessException e) {
			log_error("Group is not found");
			return null;
		} catch (Exception e) {
			log_error("Group is not found", e);
			return null;
		}
	}

	public Group findById(Integer groupId) {
		log_info("GroupId=" + groupId);
		try {
			String sql = this.sqlConfig.getSql(SQLConstant.SELECT_GROUP_BY_ID_KEY);
			log_info(SQLConstant.SELECT_GROUP_BY_ID_KEY + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));

			Object[] parms = { groupId };
			int[] types = { Types.INTEGER };

			return getJdbcTemplate().queryForObject(sql, parms, types, new GroupAllRowMapper());
		} catch (EmptyResultDataAccessException e) {
			log_error("Group is not found");
			return null;
		} catch (Exception e) {
			log_error("Group is not found", e);
			return null;
		}
	}

	public List<Group> findByLimit(GroupAllReqs request) {
		log_info("Find Group By Limit");
		String search = "%"+request.getSearch()+"%";	
		log_debug("Search = " +search);
		try {	
			String sql = this.sqlConfig.getSql(SQLConstant.SELECT_GROUP_BY_LIMIT);
			String sqlReplace = sql.replace(":wherefield", request.getWhereField());
			sqlReplace = sqlReplace.replace(":orderfield", request.getSortField()+" "+request.getSortBy().toUpperCase());
			log_info(SQLConstant.SELECT_GROUP_BY_LIMIT + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));
			log_info(sqlReplace);
			
			Object[] parms = { 
					search,
					search,
					search,
					request.getLimit(), 
					request.getOffset() 
					};

			int[] types = { 
					Types.VARCHAR,
					Types.VARCHAR,
					Types.VARCHAR,
					Types.INTEGER,
					Types.INTEGER
					};
			
			log_info("Object[] parms = " +Arrays.toString(parms));
			List<Group> list = getJdbcTemplate().query(sqlReplace, parms, types, new GroupRowMapper());
			int size = (list == null) ? 0 : list.size();
			log_debug("Group list size=" + size);
			return list;
		} catch (Exception e) {
			log_error("All Group is not found", e);
			return null;
		}
	}
	
	public List<Group> findCountAllGroup(GroupAllReqs request) {
		log_info("Find Group By Limit");
		String search = "%"+request.getSearch()+"%";	
		log_debug("Search = " +search);
		try {	
			String sql = this.sqlConfig.getSql(SQLConstant.SELECT_COUNT_ALL_GROUP);
			String sqlReplace = sql.replace(":wherefield", request.getWhereField());
			sqlReplace = sqlReplace.replace(":orderfield", request.getSortField()+" "+request.getSortBy().toUpperCase());
			log_info(SQLConstant.SELECT_COUNT_ALL_GROUP + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));
			log_info(sqlReplace);
			
			Object[] parms = { 
					search,
					search
					};

			int[] types = { 
					Types.VARCHAR,
					Types.VARCHAR
					};
			
			log_info("Object[] parms = " +Arrays.toString(parms));
			List<Group> list = getJdbcTemplate().query(sqlReplace, parms, types, new GroupCountRowMapper());
			int size = (list == null) ? 0 : list.size();
			log_debug("Group list size=" + size);
			return list;
		} catch (Exception e) {
			log_error("All Group is not found", e);
			return null;
		}
	}
//	
//	public Group findCountAllGroup(String search) {
//		log_info("Count Group By Limit");		
//		try {
//			search = "%"+search+"%";
//			String sql = this.sqlConfig.getSql(SQLConstant.SELECT_COUNT_ALL_GROUP);
//			log_info(SQLConstant.SELECT_COUNT_ALL_GROUP + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));
//			
//			Object[] parms = { 
//					search,
//					search 
//					};
//
//			int[] types = { 
//					Types.VARCHAR,
//					Types.VARCHAR		
//					};			
//		
//			Group group = getJdbcTemplate().queryForObject(sql, parms, types, new GroupCountRowMapper());
//			return group;
//		} catch (Exception e) {
//			log_error("All Group is not found", e);
//			return null;
//		}
//	}

	public void deleteById(Integer groupId) {
		log_info("Delete GroupId=" + groupId);
		String sql = this.sqlConfig.getSql(SQLConstant.DELETE_GROUP_BY_ID_KEY);
		log_info(SQLConstant.DELETE_GROUP_BY_ID_KEY + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));

		Object[] parms = { groupId };

		int[] types = { Types.INTEGER };

		int rowUpdate = getJdbcTemplate().update(sql, parms, types);
		log_info("Deleted " + rowUpdate + " row(s)");
	}

}
