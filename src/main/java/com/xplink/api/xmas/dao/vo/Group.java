package com.xplink.api.xmas.dao.vo;

import java.io.Serializable;
import java.util.Date;

public class Group implements Serializable {

	private static final long serialVersionUID = 7129904658929459073L;

	private Integer groupId;
	private String groupName;
	private Date groupDttm;
	private Integer groupState;
	private Integer groupCreatedBy;
	private String username;
	private Integer players;
	private String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getPlayers() {
		return players;
	}

	public void setPlayers(Integer players) {
		this.players = players;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Date getGroupDttm() {
		return groupDttm;
	}

	public void setGroupDttm(Date groupDttm) {
		this.groupDttm = groupDttm;
	}

	public Integer getGroupState() {
		return groupState;
	}

	public void setGroupState(Integer groupState) {
		this.groupState = groupState;
	}

	public Integer getGroupCreatedBy() {
		return groupCreatedBy;
	}

	public void setGroupCreatedBy(Integer groupCreatedBy) {
		this.groupCreatedBy = groupCreatedBy;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
