package com.xplink.api.xmas.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.List;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xplink.api.config.sql.SQLPropertyFileConfig;
import com.xplink.api.dao.AbstractDao;
import com.xplink.api.exception.ServiceException;
import com.xplink.api.rest.constant.ErrorConstant;
import com.xplink.api.rest.constant.SQLConstant;
import com.xplink.api.xmas.controller.vo.EventInfo;
import com.xplink.api.xmas.controller.vo.GroupInfo;
import com.xplink.api.xmas.controller.vo.PlayersInfo;
import com.xplink.api.xmas.dao.rowmapper.EventRowMapper;
import com.xplink.api.xmas.dao.rowmapper.PlayersRowMapper;
import com.xplink.api.xmas.dao.vo.Event;

@Repository
@Transactional
public class EventDao extends AbstractDao {

	public int create(Event event) throws ServiceException {
		log_info("Create Event=" + event.getEvtLastChngDttm());
		if(findByGroupEventId(event) != null) {
			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_EVENT_USER_EXIST, ErrorConstant.ERR_DESC_EVENT_USER_EXIST);
		}			
		log_debug("#################################################");
		String sql = this.sqlConfig.getSql(SQLConstant.INSERT_EVENT_KEY);
		log_info(SQLConstant.INSERT_EVENT_KEY + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));
		log_debug("timeStamp="+event.getEvtLastChngDttm().getTime()+" ,groupId="+event.getGroupId()+" ,userId="+event.getUserId());
		
		KeyHolder keyHolder = new GeneratedKeyHolder();
		int rowUpdate = getJdbcTemplate().update(new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int i = 1;
				ps.setTimestamp(i++, new Timestamp(event.getEvtLastChngDttm().getTime()));
				ps.setInt(i++, event.getGroupId());
				ps.setInt(i++, event.getUserId());
				return ps;
			}
		}, keyHolder);

//		int id = keyHolder.getKey().intValue();
//		log_info("Inserted " + rowUpdate + " row(s), id=" + id);
//		return id;
		
		Long newId;
	    if (keyHolder.getKeys().size() > 1) {
	        newId = (Long)keyHolder.getKeys().get("evt_id");
	    } else {
	        newId= keyHolder.getKey().longValue();
	    }
		int id = newId.intValue();
		log_info("Inserted " + rowUpdate + " row(s), id=" + id);
		return id;
	}

	public void update(Event event) {
		log_info("Update Event=" + event.getEvtLastChngDttm());
		String sql = this.sqlConfig.getSql(SQLConstant.UPDATE_EVENT_KEY);
		log_info(SQLConstant.UPDATE_EVENT_KEY + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));
		
		

		Object[] parms = { "".equals(event.getEvtKeyword().trim()) ? null : event.getEvtKeyword(), event.getEvtLastChngDttm(), event.getEvtReceiveKeyword(), event.getGroupId(), event.getUserId() };
		
	//	int[] types = { Types.VARCHAR, Types.TIMESTAMP, Types.VARCHAR, Types.INTEGER, Types.INTEGER};

		int rowUpdate = getJdbcTemplate().update(sql, parms);
		log_info("Updated " + rowUpdate + " row(s)");
	}
	
	public void updateSttsUser(EventInfo eventInfo) {
		log_info("Update Event=" + eventInfo.getLastChngDttm());
		String sql = this.sqlConfig.getSql(SQLConstant.UPDATE_EVENT_ACCEPT);
		log_info(SQLConstant.UPDATE_EVENT_ACCEPT + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));

		Object[] parms = { eventInfo.getLastChngDttm(), eventInfo.getUserStts(), eventInfo.getGroupId(), eventInfo.getUserId() };
		
		int[] types = { Types.TIMESTAMP, Types.INTEGER, Types.INTEGER, Types.INTEGER};

		int rowUpdate = getJdbcTemplate().update(sql, parms, types);
		log_info("Updated " + rowUpdate + " row(s)");
	}
	
	/*Used at RandomService*/
	public Event findById(Integer eventId) {
		log_info("EventId=" + eventId);
		try {
			String sql = this.sqlConfig.getSql(SQLConstant.SELECT_EVENT_BY_ID_KEY);
			log_info(SQLConstant.SELECT_EVENT_BY_ID_KEY + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));

			Object[] parms = { eventId };
			int[] types = { Types.INTEGER };

			return getJdbcTemplate().queryForObject(sql, parms, types, new EventRowMapper());
		} catch (EmptyResultDataAccessException e) {
			log_error("Event is not found");
			return null;
		} catch (Exception e) {
			log_error("Event is not found", e);
			return null;
		}
	}
	
	public List<PlayersInfo> findPlayersByGroup(Integer groupId) {
		log_info("Find Players by Group");
		try {
			String sql = this.sqlConfig.getSql(SQLConstant.SELECT_PLAYERS_BY_GROUP_KEY);
			log_info(SQLConstant.SELECT_PLAYERS_BY_GROUP_KEY + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));
			
			Object[] parms = {groupId};		
			int[] types = {Types.INTEGER};
			
			List<PlayersInfo> list = getJdbcTemplate().query(sql, parms, types, new PlayersRowMapper());
			int size = (list == null) ? 0 : list.size();
			log_debug("Players by Group list size=" + size);
			return list;
		} catch (Exception e) {
			log_error("Players by Group is not found", e);
			return null;
		}
	}
	
	public Event findByGroupId(Integer groupId, Integer userId) throws ServiceException {
		log_info("Find by user");
		try {
			String sql = this.sqlConfig.getSql(SQLConstant.SELECT_GROUP_EVENT_KEY);
			log_info(SQLConstant.SELECT_GROUP_EVENT_KEY + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));

			Object[] parms = { groupId, userId };
			log_debug("groupId : " + groupId + ", userId : " + userId);
			int[] types = { Types.INTEGER, Types.INTEGER };

			return getJdbcTemplate().queryForObject(sql, parms, types, new EventRowMapper());
		}catch (EmptyResultDataAccessException e) {
			log_error("Event is not found");
			return null;
//			throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_KEYWORD_NOT_FOUND, ErrorConstant.ERR_DESC_KEYWORD_NOT_FOUND);
		} catch(Exception e) {
			log_error("EventDao findByGroupId Error:", e);
			return null;
		}
	}
	
	public Event findByKeyword(Integer groupId, String keyword) {
		log_info("Find by keyword");
		try {
			String sql = this.sqlConfig.getSql(SQLConstant.SELECT_KEYWORD_BY_EVENT_GROUP_KEY);
			log_info(SQLConstant.SELECT_KEYWORD_BY_EVENT_GROUP_KEY + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));

			Object[] parms = { groupId, keyword };
			int[] types = { Types.INTEGER, Types.VARCHAR };

			return getJdbcTemplate().queryForObject(sql, parms, types, new EventRowMapper());
		}catch (EmptyResultDataAccessException e) {
			log_error("Event Keyword is not found");
			return null;
		} catch(Exception e) {
			log_error("Event Keyword is not found");
			return null;
		}
	}
	
	public Event findByGroupEventId(Event event) {
		log_info("Find by GroupId and UserId");
		try {
			String sql = this.sqlConfig.getSql(SQLConstant.SELECT_GROUP_EVENT_KEY);
			log_info(SQLConstant.SELECT_GROUP_EVENT_KEY + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));

			Object[] parms = { event.getGroupId(), event.getUserId() };
			int[] types = { Types.INTEGER, Types.INTEGER};

			return getJdbcTemplate().queryForObject(sql, parms, types, new EventRowMapper());
		}catch (EmptyResultDataAccessException e) {
			log_error("Event is not found");
			return null;
		} catch(Exception e) {
			log_error("Event is not found");
			return null;
		}
	}
	
	public Event findByGroupLimit(Integer groupId) {
		log_info("Find by GroupId Limit");
		try {
			String sql = this.sqlConfig.getSql(SQLConstant.SELECT_EVENT_BY_LIMIT_ONE);
			log_info(SQLConstant.SELECT_EVENT_BY_LIMIT_ONE + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));

			Object[] parms = { groupId };
			int[] types = { Types.INTEGER};

			return getJdbcTemplate().queryForObject(sql, parms, types, new EventRowMapper());
		}catch (EmptyResultDataAccessException e) {
			log_error("Event is not found");
			return null;
		} catch(Exception e) {
			log_error("Event is not found");
			return null;
		}		
	}
	
	public List<Event> findByGroupId(Integer groupId) {
		log_info("Find by GroupId");
		try {
			String sql = this.sqlConfig.getSql(SQLConstant.SELECT_EVENT_BY_GROUP_KEY);
			log_info(SQLConstant.SELECT_EVENT_BY_GROUP_KEY + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));

			Object[] parms = { groupId };
			int[] types = { Types.INTEGER};

			return getJdbcTemplate().query(sql, parms, types, new EventRowMapper());
		}catch (EmptyResultDataAccessException e) {
			log_error("Event is not found");
			return null;
		} catch(Exception e) {
			log_error("Event is not found");
			return null;
		}
	}

	/* ADMIN */
	public void deleteByGroupId(Integer groupId) {
		log_info("Delete Event by groupId=" + groupId);
		String sql = this.sqlConfig.getSql(SQLConstant.DELETE_EVENT_BY_GROUP_ID_KEY);
		log_info(SQLConstant.DELETE_EVENT_BY_GROUP_ID_KEY + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));

		Object[] parms = { groupId };

		int[] types = { Types.INTEGER };

		int rowUpdate = getJdbcTemplate().update(sql, parms, types);
		log_info("Deleted " + rowUpdate + " row(s)");
	}
	
	public void deleteByUserId(EventInfo eventInfo) {
		log_info("Delete Event by userId=" + eventInfo.getUserId());
		String sql = this.sqlConfig.getSql(SQLConstant.DELETE_EVENT_BY_USER_ID_KEY);
		log_info(SQLConstant.DELETE_EVENT_BY_USER_ID_KEY + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));

		Object[] parms = { eventInfo.getGroupId(), eventInfo.getUserId() };

		int[] types = { Types.INTEGER, Types.INTEGER };

		int rowUpdate = getJdbcTemplate().update(sql, parms, types);
		log_info("Deleted " + rowUpdate + " row(s)");
	}
	/*used at RandomService*/
	public List<Event> findEventByGroup(GroupInfo groupInfo) {
		log_info("Find Event by Group");
		try {
			String sql = this.sqlConfig.getSql(SQLConstant.SELECT_EVENT_BY_GROUP_KEY);
			log_info(SQLConstant.SELECT_EVENT_BY_GROUP_KEY + "=" + SQLPropertyFileConfig.removeNewLineAndTab(sql));
			Object[] parms = { groupInfo.getGroupId() };
			int[] types = { Types.INTEGER };
			List<Event> list = getJdbcTemplate().query(sql, parms, types, new EventRowMapper());			
			return list;
		} catch (Exception e) {
			log_error("Event is not found", e);
			return null;
		}
	}
}
