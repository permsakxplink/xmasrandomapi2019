package com.xplink.api.xmas.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.xplink.api.xmas.dao.vo.Event;

public class EventRowMapper implements RowMapper<Event>{
	
	@Override
	public Event mapRow(ResultSet rs, int rowNum) throws SQLException {
		Event evt = new Event();
		evt.setEvtId(rs.getInt("evt_id"));
		evt.setEvtKeyword(rs.getString("evt_kywd"));
		evt.setEvtLastChngDttm(rs.getTimestamp("evt_last_chng_dttm"));
		evt.setEvtReceiveKeyword(rs.getString("evt_receive_keyword"));
		evt.setGroupId(rs.getInt("xmas_group_grp_id"));
		evt.setUserId(rs.getInt("xmas_user_usr_id"));
//		evt.setUserStts(rs.getInt("xmas_user_stts"));
		evt.setUserStts(1);
		return evt;
	}
}
