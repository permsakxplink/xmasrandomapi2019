package com.xplink.api.log;

import java.text.MessageFormat;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class AbstractLogManager {

	protected Logger logger = LogManager.getLogger(this.getClass());

	protected void log_info(String wording, Object... arguments) {
		String mesg = MessageFormat.format(wording, arguments);
		log_info(mesg);
	}

	protected void log_debug(String wording, Object... arguments) {
		String mesg = MessageFormat.format(wording, arguments);
		log_debug(mesg);
	}

	protected void log_warn(String wording, Object... arguments) {
		String mesg = MessageFormat.format(wording, arguments);
		log_warn(mesg);
	}

	protected void log_warn(String mesg) {
		this.logger.warn(mesg);
	}

	protected void log_info(String mesg) {
		this.logger.info(mesg);
	}

	protected void log_debug(String mesg) {
		this.logger.debug(mesg);
	}

	protected void log_error(String mesg) {
		this.logger.error(mesg);
	}

	protected void log_error(String mesg, Throwable e) {
		log_error(mesg);
		logTrowable(e);
	}

	private void logTrowable(Throwable cause) {
		String prefix = "Cause by " + cause.getClass().getName() + ": ";
		String message = cause.getMessage();
		if (message != null) {
			try (Scanner sc = new Scanner(message)) {
				while (sc.hasNext()) {
					log_error(prefix + sc.nextLine());
					prefix = "  ";
				}
			} catch (Throwable e) {
				this.logger.error("Cannot logging error with Scanner class", e);
			}
		} else {
			log_error(prefix);
		}
		StackTraceElement[] traces = cause.getStackTrace();
		for (StackTraceElement trace : traces) {
			log_error(toStackTraceString(trace));
		}
		if (cause.getCause() != null) {
			logTrowable(cause.getCause());
		}
	}

	private String toStackTraceString(StackTraceElement trace) {
		return "  at " + trace.getClassName() + "." + trace.getMethodName() + " (" + trace.getFileName() + ":" + trace.getLineNumber() + ")";
	}
}