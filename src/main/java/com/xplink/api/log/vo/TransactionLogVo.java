package com.xplink.api.log.vo;

import java.io.Serializable;

public class TransactionLogVo implements Serializable {

	private static final long serialVersionUID = -4792731975928415987L;
	private static final String PIPE = "|";

	private long startTimestamp;
	private String uuid;
	private String bizService;
	private Integer status;
	private String errorCode;
	private String httpMethod;
	private String requestIp;

	public String getHttpMethod() {
		return httpMethod;
	}

	public void setHttpMethod(String httpMethod) {
		this.httpMethod = httpMethod;
	}

	public long getStartTimestamp() {
		return startTimestamp;
	}

	public void setStartTimestamp(long startTimestamp) {
		this.startTimestamp = startTimestamp;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getBizService() {
		return bizService;
	}

	public void setBizService(String bizService) {
		this.bizService = bizService;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getRequestIp() {
		return requestIp;
	}

	public void setRequestIp(String requestIp) {
		this.requestIp = requestIp;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(bizService);
		builder.append(PIPE);
		builder.append(httpMethod);
		builder.append(PIPE);
		builder.append(requestIp);
		builder.append(PIPE);
		builder.append(status);
		builder.append(PIPE);
		builder.append(errorCode);
		
		long elapsedTime = System.currentTimeMillis() - startTimestamp;
		builder.append(PIPE);
		builder.append(elapsedTime);
		builder.append(PIPE);
		builder.append(uuid);
		return builder.toString().replaceAll("null", "");
	}

}
