package com.xplink.api.rest.controller.vo;

import java.io.Serializable;

public class RestHeaderRequest implements Serializable {

	private static final long serialVersionUID = -2861075563197919286L;

	private Long timestamp;
	private String uuid;
	private String seriesId;
	private String token;
	private String bizService;
	private String origin;
	private String requestURL;
	private String remoteHost;

	public String getRemoteHost() {
		return remoteHost;
	}

	public void setRemoteHost(String remoteHost) {
		this.remoteHost = remoteHost;
	}

	public String getRequestURL() {
		return requestURL;
	}

	public void setRequestURL(String requestURL) {
		this.requestURL = requestURL;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getSeriesId() {
		return seriesId;
	}

	public void setSeriesId(String seriesId) {
		this.seriesId = seriesId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getBizService() {
		return bizService;
	}

	public void setBizService(String bizService) {
		this.bizService = bizService;
	}

}
