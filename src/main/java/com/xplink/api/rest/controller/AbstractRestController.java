package com.xplink.api.rest.controller;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.digest.Md5Crypt;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.xplink.api.exception.ServiceException;
import com.xplink.api.log.AbstractLogManager;
import com.xplink.api.log.vo.TransactionLogVo;
import com.xplink.api.rest.constant.Constant;
import com.xplink.api.rest.constant.ErrorConstant;
import com.xplink.api.rest.controller.vo.GenericRestResponse;
import com.xplink.api.rest.controller.vo.RestHeaderRequest;
import com.xplink.api.xmas.service.AuthorizeService;

@CrossOrigin(origins = "*", maxAge = 3600)
public abstract class AbstractRestController extends AbstractLogManager {

	@Value("${app.name}")
	private String appName;
	
	@Autowired
	protected HttpServletRequest httpServletContext;
	
	@Autowired
	protected Gson gson;
	
	@Autowired
	protected AuthorizeService authorizeService;
	
	@Autowired
	@Qualifier("loggerTx")
	private Logger loggerTx;
	
	@Bean(name="loggerTx")
	private Logger loggerTx() {
		String loggerTxName = this.appName + "_Tx";
		return LogManager.getLogger(loggerTxName);
	}
	
	protected abstract String get(RestHeaderRequest header, Map<String, String> dataMap) throws ServiceException;
	protected abstract String post(RestHeaderRequest header, String request) throws ServiceException;
	protected abstract String put(RestHeaderRequest header, String request) throws ServiceException;
	protected abstract String delete(RestHeaderRequest header, String request) throws ServiceException;
	
	private Map<String, String> buildGetRequest(RestHeaderRequest header, HttpServletRequest httpRequest) {
		Map<String, String> dataMap = new HashMap<String, String>();
		Map<String, String[]> paramMap = httpRequest.getParameterMap();
		Set<Entry<String, String[]>> set = paramMap.entrySet();
		for (Entry<String, String[]> entry : set) {
			String key = entry.getKey();
			String[] values = entry.getValue();
			dataMap.put(key, values[0]);
		}
		
		return dataMap;
	}
	
	private String getUrlParamString(HttpServletRequest httpRequest) {
		StringBuilder bodyBuilder = new StringBuilder();
		Enumeration<String> params = httpRequest.getParameterNames(); 
		while(params.hasMoreElements()){
		 String paramName = params.nextElement();
		 bodyBuilder.append(paramName).append("=").append(httpRequest.getParameter(paramName)).append(",");
		}
		
		return bodyBuilder.length() == 0 ? bodyBuilder.toString() : (bodyBuilder.substring(0, (bodyBuilder.length() - 1)));
	}
	
	@RequestMapping(method = RequestMethod.GET, produces="application/json")
    @ResponseBody
    public ResponseEntity<String> get(HttpServletRequest httpRequest) throws ServiceException {
		long startTime = System.currentTimeMillis();
		TransactionLogVo logVo = new TransactionLogVo();
		buildTransactionLogVo(logVo, httpRequest);
		String httpMethod = httpRequest.getMethod();
		initLogParam(httpRequest);
		String response = null;
		log_info("Start process with method " + httpMethod);
		RestHeaderRequest header = null;
		try {			
			
			header = buildHeaderRequest(httpRequest);
			log_info("Receive=" + getEncryptData(header, getUrlParamString(httpRequest)));
			
			validateAuthorize(header);
			
			Map<String, String> dataMap = buildGetRequest(header, httpRequest);

			response = get(header, dataMap);
			if ( response == null ) {
				return new ResponseEntity<String>(HttpStatus.METHOD_NOT_ALLOWED);
			}
			logVo.setStatus(ErrorConstant.STTS_SUCC);
			return new ResponseEntity<String>(response, HttpStatus.OK);
			
		} catch (ServiceException e) {
			log_error("SERVICE_NAME:" + e.getServiceName() + "|ERRCODE:" + e.getErrorCode() + "|ERRMESSAGE:" + e.getMessage() + "|workingTime = " + (System.currentTimeMillis() - startTime));
			ResponseEntity<String> errorResponse = buildErrorResponse(logVo, e.getErrorCode(), e.getMessage());
			if("B1002".equals(e.getErrorCode())) {
				return  new ResponseEntity<String>(errorResponse.getBody(), HttpStatus.UNAUTHORIZED);
			} else if ("B1003".equals(e.getErrorCode())){
				return  new ResponseEntity<String>(errorResponse.getBody(), HttpStatus.FORBIDDEN);
			} else {
//			return buildErrorResponse(logVo, e.getErrorCode(), e.getMessage());
			return errorResponse;
			}
		} catch (Throwable e) {
			String errorDesc = ErrorConstant.ERR_DESC_UNEXPECTED + ", message=" + e.getMessage();
			log_error("SERVICE_NAME:" + this.getClass().getSimpleName() + "|ERRCODE:" + ErrorConstant.ERR_CODE_UNEXPECTED + "|ERRMESSAGE:" + errorDesc + "|workingTime = " + (System.currentTimeMillis() - startTime), e);
			return buildErrorResponse(logVo, ErrorConstant.ERR_CODE_UNEXPECTED, errorDesc);
		} finally {
			
			this.loggerTx.info(logVo.toString());
			log_info("Return=" + getEncryptData(header, response));
			log_info("End process with method " + httpMethod + ", elapsed time = " + (System.currentTimeMillis() - startTime));
			clearLogParam();
		}
		/*
		RestHeaderRequest header = null;
		header = buildHeaderRequest(httpRequest);
		Map<String, String> dataMap = buildGetRequest(header, httpRequest);
		log_info("Receive=" + getEncryptData(header, getUrlParamString(httpRequest)));
		return new ResponseEntity<String>(get(header, dataMap),HttpStatus.OK);
		*/
    }
	
	@RequestMapping(method = RequestMethod.POST, produces={"application/json"}, consumes="application/json")
    @ResponseBody
    public ResponseEntity<String> post(HttpServletRequest httpRequest, @RequestBody String request) throws ServiceException {
		long startTime = System.currentTimeMillis();
		TransactionLogVo logVo = new TransactionLogVo();
		buildTransactionLogVo(logVo, httpRequest);
		String httpMethod = httpRequest.getMethod();
		initLogParam(httpRequest);
		String response = null;
		log_info("Start process with methodx " + httpMethod);
		RestHeaderRequest header = null;
		try {
			
			if ( request == null || "".equals(request.trim()) ) {
				log_info("Receive=" + request);
				throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_REQS_BODY_NULL, ErrorConstant.ERR_DESC_REQS_BODY_NULL);
			}
			
			header = buildHeaderRequest(httpRequest);
			String reqsData = request.replaceAll("(\r\n|\r|\n|\n\r)", "").replaceAll(",\\s*\"", ", \"");
			log_info("Receive=" + getEncryptData(header, reqsData));
			
			validateAuthorize(header);
			log_info("test");	
			response = post(header, request);
			if ( response == null ) {
				return new ResponseEntity<String>(HttpStatus.METHOD_NOT_ALLOWED);
			}
			
			logVo.setStatus(ErrorConstant.STTS_SUCC);
			log_info(response);
			return new ResponseEntity<String>(response, HttpStatus.OK);
		
		} catch (ServiceException e) {
			log_error("SERVICE_NAME:" + e.getServiceName() + "|ERRCODE:" + e.getErrorCode() + "|ERRMESSAGE:" + e.getMessage() + "|workingTime = " + (System.currentTimeMillis() - startTime));			
			ResponseEntity<String> errorResponse = buildErrorResponse(logVo, e.getErrorCode(), e.getMessage());
			if("B1002".equals(e.getErrorCode())) {
				return  new ResponseEntity<String>(errorResponse.getBody(), HttpStatus.UNAUTHORIZED);
			} else if ("B1003".equals(e.getErrorCode())){
				return  new ResponseEntity<String>(errorResponse.getBody(), HttpStatus.FORBIDDEN);
			} else {
//				return buildErrorResponse(logVo, e.getErrorCode(), e.getMessage());
				return errorResponse;
			}
		} catch (Throwable e) {
			String errorDesc = ErrorConstant.ERR_DESC_UNEXPECTED + ", message=" + e.getMessage();
			log_error("SERVICE_NAME:" + this.getClass().getSimpleName() + "|ERRCODE:" + ErrorConstant.ERR_CODE_UNEXPECTED + "|ERRMESSAGE:" + errorDesc + "|workingTime = " + (System.currentTimeMillis() - startTime), e);
			return buildErrorResponse(logVo, ErrorConstant.ERR_CODE_UNEXPECTED, errorDesc);
		} finally {
			
			this.loggerTx.info(logVo.toString());
			log_info("Return=" + getEncryptData(header, response));
			log_info("End process with method " + httpMethod + ", elapsed time = " + (System.currentTimeMillis() - startTime));
			clearLogParam();
		}
    }
	
	@RequestMapping(method = RequestMethod.PUT, produces={"application/json"}, consumes="application/json")
    @ResponseBody
    public ResponseEntity<String> put(HttpServletRequest httpRequest, @RequestBody String request) {
		long startTime = System.currentTimeMillis();
		TransactionLogVo logVo = new TransactionLogVo();
		buildTransactionLogVo(logVo, httpRequest);
		String httpMethod = httpRequest.getMethod();
		initLogParam(httpRequest);
		String response = null;
		log_info("Start process with method " + httpMethod);
		RestHeaderRequest header = null;
		try {
			
			if ( request == null || "".equals(request.trim()) ) {
				log_info("Receive=" + request);
				throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_REQS_BODY_NULL, ErrorConstant.ERR_DESC_REQS_BODY_NULL);
			}
			
			header = buildHeaderRequest(httpRequest);
			String reqsData = request.replaceAll("(\r\n|\r|\n|\n\r)", "").replaceAll(",\\s*\"", ", \"");
			log_info("Receive=" + getEncryptData(header, reqsData));
			
			validateAuthorize(header);
			
			response = put(header, request);
			if ( response == null ) {
				return new ResponseEntity<String>(HttpStatus.METHOD_NOT_ALLOWED);
			}
			
			logVo.setStatus(ErrorConstant.STTS_SUCC);
			return new ResponseEntity<String>(response, HttpStatus.OK);
		
		} catch (ServiceException e) {
			log_error("SERVICE_NAME:" + e.getServiceName() + "|ERRCODE:" + e.getErrorCode() + "|ERRMESSAGE:" + e.getMessage() + "|workingTime = " + (System.currentTimeMillis() - startTime));
			ResponseEntity<String> errorResponse = buildErrorResponse(logVo, e.getErrorCode(), e.getMessage());
			if("B1002".equals(e.getErrorCode())) {
				return  new ResponseEntity<String>(errorResponse.getBody(), HttpStatus.UNAUTHORIZED);
			} else if ("B1003".equals(e.getErrorCode())){
				return  new ResponseEntity<String>(errorResponse.getBody(), HttpStatus.FORBIDDEN);
			} else {
//			return buildErrorResponse(logVo, e.getErrorCode(), e.getMessage());
				return errorResponse;
			}
		} catch (Throwable e) {
			String errorDesc = ErrorConstant.ERR_DESC_UNEXPECTED + ", message=" + e.getMessage();
			log_error("SERVICE_NAME:" + this.getClass().getSimpleName() + "|ERRCODE:" + ErrorConstant.ERR_CODE_UNEXPECTED + "|ERRMESSAGE:" + errorDesc + "|workingTime = " + (System.currentTimeMillis() - startTime), e);
			return buildErrorResponse(logVo, ErrorConstant.ERR_CODE_UNEXPECTED, errorDesc);
		} finally {
			
			this.loggerTx.info(logVo.toString());
			log_info("Return=" + getEncryptData(header, response));
			log_info("End process with method " + httpMethod + ", elapsed time = " + (System.currentTimeMillis() - startTime));
			clearLogParam();
		}
    }
	
	@RequestMapping(method = RequestMethod.DELETE, produces={"application/json"}, consumes="application/json")
    @ResponseBody
    public ResponseEntity<String> delete(HttpServletRequest httpRequest, @RequestBody String request) {
		long startTime = System.currentTimeMillis();
		TransactionLogVo logVo = new TransactionLogVo();
		buildTransactionLogVo(logVo, httpRequest);
		String httpMethod = httpRequest.getMethod();
		initLogParam(httpRequest);
		String response = null;
		log_info("Start process with method " + httpMethod);
		RestHeaderRequest header = null;
		try {
			
			if ( request == null || "".equals(request.trim()) ) {
				log_info("Receive=" + request);
				throw new ServiceException(this.getClass().getSimpleName(), ErrorConstant.ERR_CODE_REQS_BODY_NULL, ErrorConstant.ERR_DESC_REQS_BODY_NULL);
			}
			
			header = buildHeaderRequest(httpRequest);
			String reqsData = request.replaceAll("(\r\n|\r|\n|\n\r)", "").replaceAll(",\\s*\"", ", \"");
			log_info("Receive=" + getEncryptData(header, reqsData));
			
			validateAuthorize(header);
			
			response = delete(header, request);
			if ( response == null ) {
				return new ResponseEntity<String>(HttpStatus.METHOD_NOT_ALLOWED);
			}
			
			logVo.setStatus(ErrorConstant.STTS_SUCC);
			return new ResponseEntity<String>(response, HttpStatus.OK);
			
		} catch (ServiceException e) {
			log_error("SERVICE_NAME:" + e.getServiceName() + "|ERRCODE:" + e.getErrorCode() + "|ERRMESSAGE:" + e.getMessage() + "|workingTime = " + (System.currentTimeMillis() - startTime));
			ResponseEntity<String> errorResponse = buildErrorResponse(logVo, e.getErrorCode(), e.getMessage());
			if("B1002".equals(e.getErrorCode())) {
				return  new ResponseEntity<String>(errorResponse.getBody(), HttpStatus.UNAUTHORIZED);
			} else if ("B1003".equals(e.getErrorCode())){
				return  new ResponseEntity<String>(errorResponse.getBody(), HttpStatus.FORBIDDEN);
			} else {
//			return buildErrorResponse(logVo, e.getErrorCode(), e.getMessage());
				return errorResponse;
			}
		} catch (Throwable e) {
			String errorDesc = ErrorConstant.ERR_DESC_UNEXPECTED + ", message=" + e.getMessage();
			log_error("SERVICE_NAME:" + this.getClass().getSimpleName() + "|ERRCODE:" + ErrorConstant.ERR_CODE_UNEXPECTED + "|ERRMESSAGE:" + errorDesc + "|workingTime = " + (System.currentTimeMillis() - startTime), e);
			return buildErrorResponse(logVo, ErrorConstant.ERR_CODE_UNEXPECTED, errorDesc);
		} finally {
			
			this.loggerTx.info(logVo.toString());
			log_info("Return=" + getEncryptData(header, response));
			log_info("End process with method " + httpMethod + ", elapsed time = " + (System.currentTimeMillis() - startTime));
			clearLogParam();
		}
    }
	
	private RestHeaderRequest buildHeaderRequest(HttpServletRequest httpRequest) {
		RestHeaderRequest header = new RestHeaderRequest();
		header.setUuid(httpRequest.getHeader("uuid"));
		header.setBizService(httpRequest.getHeader("bizService"));
		header.setSeriesId(httpRequest.getHeader("seriesId"));
		header.setToken(httpRequest.getHeader("token"));
		header.setRemoteHost(httpRequest.getRemoteHost());
		
		if (httpRequest.getHeader("Origin") == null) {
			header.setOrigin(null);
		} else {
			int lastIndex = httpRequest.getHeader("Origin").lastIndexOf(":");
			header.setOrigin(httpRequest.getHeader("Origin").substring(0, lastIndex));
		}
		
		long timestamp = System.currentTimeMillis();
		try {
			timestamp = Long.parseLong(httpRequest.getHeader("timestamp"));
		} catch (Exception e) {}
		header.setTimestamp(timestamp);
		return header;
	}
	
	private void validateAuthorize(RestHeaderRequest header) throws ServiceException {
		if ( "authen".equals(header.getBizService()) 
				|| "create-user".equals(header.getBizService())
				|| "logout".equals(header.getBizService())
				|| "forget-password".equals(header.getBizService())) {
			return;
		}
		
		String seriesId = header.getSeriesId();
		String token = header.getToken();
		this.authorizeService.updateToken(seriesId, token);
	}
	
	private ResponseEntity<String> buildErrorResponse(TransactionLogVo logVo, String errorCode, String errorDesc) {
		
		if ( ErrorConstant.ERR_CODE_WRONG_PASS.equals(errorCode) ) {
			return new ResponseEntity<String>(HttpStatus.UNAUTHORIZED);
		}
		
		GenericRestResponse<String> response = new GenericRestResponse<String>();
		response.setStatus(ErrorConstant.STTS_ERROR);
		response.setErrorCode(errorCode);
		response.setErrorDesc(errorDesc);
		
		logVo.setStatus(ErrorConstant.STTS_ERROR);
		logVo.setErrorCode(errorCode);
		
		return new ResponseEntity<String>(this.gson.toJson(response), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	private void buildTransactionLogVo(TransactionLogVo logVo, HttpServletRequest httpRequest) {
		
		String uuid = httpRequest.getHeader("uuid");
		String bizService = httpRequest.getHeader("bizService");
		String method = httpRequest.getMethod();
		String requestIp = httpRequest.getRemoteAddr();
		
		logVo.setStartTimestamp(System.currentTimeMillis());
		logVo.setUuid(uuid);
		logVo.setBizService(bizService);
		logVo.setHttpMethod(method);
		logVo.setRequestIp(requestIp);
	}
	
	private void initLogParam(HttpServletRequest httpRequest) {

		String uuid = httpRequest.getHeader("uuid");
		uuid = StringUtils.isEmpty(uuid) ? Constant.LOG_PARM_UNKNOWN_VAL : uuid;
		String bizService = httpRequest.getHeader("bizService");
		bizService = StringUtils.isEmpty(bizService) ? Constant.LOG_PARM_UNKNOWN_VAL : bizService;
		String method = httpRequest.getMethod();
		
		ThreadContext.put(Constant.LOG_PARM_KEY_UUID, uuid);
		ThreadContext.put(Constant.LOG_PARM_KEY_BIZ_SERVICE, bizService);
		ThreadContext.put(Constant.LOG_PARM_KEY_HTTP_METHOD, method);
	}
	
	private void clearLogParam() {
		
		ThreadContext.remove(Constant.LOG_PARM_KEY_UUID);
		ThreadContext.remove(Constant.LOG_PARM_KEY_BIZ_SERVICE);
		ThreadContext.remove(Constant.LOG_PARM_KEY_HTTP_METHOD);
	}
	
	private String getEncryptData(RestHeaderRequest header, String data) {
		if ( header == null ) {
			return data;
		}
		
		if ( data == null ) {
			return data;
		}
		
		if ( header.getBizService() == null ) {
			log_error("BizService in Header is null");
			return data;
		}
		
		if ( header.getBizService().indexOf("kywd") > -1 || header.getBizService().indexOf("authen") > -1) {
			return Md5Crypt.md5Crypt(data.getBytes());
		} else {
			return data;
		}
	}
}
