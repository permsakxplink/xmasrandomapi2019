package com.xplink.api.rest.constant;

public class SQLConstant {
	
	private SQLConstant() {}

	public static final String SELECT_PERSIT_TOKEN_KEY = "SELECT_PERSIT_TOKEN";
	public static final String INSERT_PERSIT_TOKEN_KEY = "INSERT_PERSIT_TOKEN";
	public static final String DELETE_PERSIT_TOKEN_KEY = "DELETE_PERSIT_TOKEN";
	public static final String UPDATE_PERSIT_TOKEN_KEY = "UPDATE_PERSIT_TOKEN";
	public static final String SELECT_PERSIT_TOKEN_BY_USERNAME_KEY = "SELECT_PERSIT_TOKEN_BY_USERNAME";
	public static final String SELECT_PERSIT_ROLE_BY_TOKEN = "SELECT_PERSIT_ROLE_BY_TOKEN";
	
	public static final String INSERT_USER_KEY = "INSERT_USER";
	public static final String UPDATE_USER_KEY = "UPDATE_USER";
	public static final String SELECT_USER_BY_NAME_KEY = "SELECT_USER_BY_NAME";
	public static final String SELECT_USER_BY_ID_KEY = "SELECT_USER_BY_ID";
	public static final String SELECT_ALL_USER_KEY = "SELECT_ALL_USER";
	public static final String SELECT_PLAYERS_BY_GROUP_KEY = "SELECT_PLAYERS_BY_GROUP";
	public static final String DELETE_USER_BY_ID_KEY = "DELETE_USER_BY_ID";
	public static final String SELECT_USER_BY_FULL_NAME = "SELECT_USER_BY_FULL_NAME";
	
	public static final String INSERT_RANDOM_KEY = "INSERT_RANDOM";
	public static final String SELECT_RAND_NOW_KEY = "SELECT_RAND_NOW";
	public static final String UPDATE_RAND_CANCEL_KEY = "UPDATE_RAND_CANCEL";
	
	public static final String INSERT_KEYWORD_KEY = "INSERT_KEYWORD";
	public static final String UPDATE_KEYWORD_KEY = "UPDATE_KEYWORD";
	public static final String SELECT_KEYWORD_BY_NAME_KEY = "SELECT_KEYWORD_BY_NAME";
	public static final String SELECT_ALL_KEYWORD_KEY = "SELECT_ALL_KEYWORD";
	public static final String DELETE_KEYWORD_BY_USER_KEY = "DELETE_KEYWORD_BY_USER";
	
	public static final String CREATE_EVENT_RANDOM_KEY = "CREATE_EVENT_RANDOM";
	public static final String SELECT_EVENT_BY_NAME_KEY = "SELECT_EVENT_BY_NAME";
	
	public static final String SELECT_COUNT_ALL_GROUP = "SELECT_COUNT_ALL_GROUP";
	public static final String INSERT_GROUP_KEY = "INSERT_GROUP";
	public static final String UPDATE_GROUP_KEY = "UPDATE_GROUP";
	public static final String UPDATE_GROUP_CREATE_BY = "UPDATE_GROUP_CREATE_BY";
	public static final String UPDATE_GROUP_STATE = "UPDATE_GROUP_STATE";
	public static final String UPDATE_GROUP_NOTE = "UPDATE_GROUP_NOTE";
	public static final String SELECT_GROUP_BY_NAME_KEY = "SELECT_GROUP_BY_NAME";
	public static final String SELECT_GROUP_BY_ID_KEY = "SELECT_GROUP_BY_ID";
	public static final String SELECT_GROUP_BY_LIMIT = "SELECT_GROUP_BY_LIMIT";
	public static final String DELETE_GROUP_BY_ID_KEY = "DELETE_GROUP_BY_ID";
	
	public static final String INSERT_EVENT_KEY = "INSERT_EVENT";
	public static final String UPDATE_EVENT_KEY = "UPDATE_EVENT";
	public static final String UPDATE_EVENT_ACCEPT = "UPDATE_EVENT_ACCEPT";
	public static final String SELECT_EVENT_BY_ID_KEY = "SELECT_EVENT_BY_ID";
	public static final String SELECT_EVENT_BY_LIMIT_ONE = "SELECT_EVENT_BY_GROUP_ID";
	public static final String DELETE_EVENT_BY_GROUP_ID_KEY = "DELETE_EVENT_BY_GROUP_ID";
	public static final String DELETE_EVENT_BY_USER_ID_KEY = "DELETE_EVENT_BY_USER_ID";
	public static final String SELECT_KEYWORD_BY_EVENT_GROUP_KEY = "SELECT_KEYWORD_BY_GROUP_EVENT";
	public static final String SELECT_GROUP_EVENT_KEY = "SELECT_GROUP_EVENT";
	public static final String SELECT_EVENT_BY_GROUP_KEY = "SELECT_EVENT_BY_GROUP";
}
